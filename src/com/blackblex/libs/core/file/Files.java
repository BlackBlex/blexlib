/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.file
 *
 * ==============Information==============
 *      Filename: Files.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.file;

import com.blackblex.libs.core.interfaces.file.FileReadInterface;
import com.blackblex.libs.core.interfaces.file.FileWriteInterface;
import com.blackblex.libs.main.Init;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Files implements FileReadInterface, FileWriteInterface
{

	private String fileDir;
	private File fileData = null;

	public Files()
	{
	}

	public void init(String file)
	{
		this.fileDir = file;
		this.fileData = new File(this.fileDir);
	}

	public void changeFile(String file)
	{
		init(file);
	}

	public boolean delete()
	{
		return this.fileData.delete();
	}

	public boolean mkdir()
	{
		return this.fileData.mkdir();
	}

	public boolean exist()
	{
		return this.fileData.exists();
	}

	public boolean isDir()
	{
		return this.fileData.isDirectory();
	}

	public String getFileName()
	{
		return this.fileData.getName();
	}

	@Override
	public String read()
	{
		String text = null;
		try
		{
			text = readFile(0);
		}
		catch ( FileNotFoundException ex )
		{
			Init.MSG.printMsg("El archivo " + this.fileData.getPath() + " no existe");
		}

		return text;
	}

	@Override
	public String readLine(int line)
	{
		String text = null;
		try
		{
			text = readFile(line);
		}
		catch ( FileNotFoundException ex )
		{
			Init.MSG.printMsg("El archivo " + this.fileData.getPath() + " no existe");
		}

		return text;
	}

	private String readFile(int line) throws FileNotFoundException
	{
		FileReader fileReader = new FileReader(this.fileData);
		BufferedReader buffer = new BufferedReader(fileReader);
		String tempLine;
		StringBuilder temp = new StringBuilder();

		try
		{
			int lineCounter = 1;
			while ( ((tempLine = buffer.readLine()) != null) )
			{
				if ( line == 0 )
					temp.append(tempLine).append("\n");
				else
				{
					if ( line == lineCounter )
					{
						temp.append(tempLine);
						break;
					}
				}
				lineCounter++;
			}
			buffer.close();
			fileReader.close();
		}
		catch ( IOException ex )
		{
			Init.MSG.printMsg("Error al leer el archivo");
		}

		return temp.toString();
	}

	@Override
	public void write(String txt, boolean newLine)
	{
		writeTxt(txt, false, true);
	}

	@Override
	public void append(String txt, boolean newLine)
	{
		writeTxt(txt, true, true);
	}

	@Override
	public void write(String txt)
	{
		writeTxt(txt, false, false);
	}

	@Override
	public void append(String txt)
	{
		writeTxt(txt, true, false);
	}

	private void writeTxt(String txt, boolean append, boolean newLine)
	{

		try
		{
			FileWriter fileWriter = new FileWriter(this.fileData, append);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.print(txt);
			if ( newLine )
			{
				printWriter.println();
			}
			printWriter.close();
			fileWriter.close();
		}
		catch ( IOException ex )
		{
			Init.MSG.printMsg("Error al escribir el archivo");
		}
	}

}
