/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.file
 *
 * ==============Information==============
 *      Filename: FileProperties.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class FileProperties
{

	private Properties properties;
	private String filePath;

	public FileProperties(String filePath)
	{
		this.filePath = filePath;
		this.properties = new Properties();
		InputStream inputStream = null;

		try
		{

			inputStream = new FileInputStream(filePath);
			this.properties.load(inputStream);

		}
		catch ( IOException ex )
		{
			ex.printStackTrace();
		}
		finally
		{
			if ( inputStream != null )
			{
				try
				{
					inputStream.close();
				}
				catch ( IOException e )
				{
					e.printStackTrace();
				}
			}
		}
	}

	public String getString(String key)
	{
		return this.properties.getProperty(key);
	}

	public int getValue(String key)
	{
		return Integer.parseInt(this.properties.getProperty(key));
	}

	public void setValue(String key, String value)
	{
		this.properties.setProperty(key, value);
	}

	public void setValue(String key, int value)
	{
		this.properties.setProperty(key, Integer.toString(value));
	}

	public boolean save()
	{
		boolean state = false;
		try
		{
			File file = new File(this.filePath);
			OutputStream outStream = new FileOutputStream(file);
			this.properties.store(outStream, "");
			state = true;
			outStream.close();
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		}

		return state;
	}

}
