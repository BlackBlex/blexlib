/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.interfaces.draw
 *
 * ==============Information==============
 *      Filename: CanvasInterface.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.interfaces.draw;

public interface CanvasInterface
{

	public void Init(int width, int height);

	public int getHeight();

	public int getWidth();

	public char [] [] getCanvasArray();

	public String paint();

	public void setPen(int x, int y, char c);

}
