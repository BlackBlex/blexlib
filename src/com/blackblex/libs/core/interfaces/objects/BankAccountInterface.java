/**
 * BlexLib
 *
 * @author  Jovani Perez Damien (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.interfaces.objects
 *
 * ==============Information==============
 *      Filename: BankAccountInterface.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.interfaces.objects;

public interface BankAccountInterface
{

	long getAccountID();

	void setAccountID(long accountid);

	String getAccountType();

	void setAccountType(String accounttype);

	String getBank();

	void setBank(String bank);

	double getBalance();

	void setBalance(double balance);

	String getDate();

	void setDate(String date);

	void deposit(double money);

	void withdrawal(double money);

}
