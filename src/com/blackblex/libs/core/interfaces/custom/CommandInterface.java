/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.interfaces.custom
 *
 * ==============Information==============
 *      Filename: CommandInterface.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.interfaces.custom;

import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;

public interface CommandInterface
{

	String getDescription();

	boolean canExecute(SocketUsername socketUsername);

	void execute(SocketUsername socketUsername, SocketMessage dataInput);

}
