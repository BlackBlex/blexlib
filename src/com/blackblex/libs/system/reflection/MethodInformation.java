/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.reflection
 *
 * ==============Information==============
 *      Filename: MethodInformation.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.reflection;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class MethodInformation
{
	private Object objectInformation;
	private Method methodInformation;

	public MethodInformation(Method method, Object obj)
	{
		this.methodInformation = method;
		this.objectInformation = obj;
	}

	public String getName()
	{
		return this.methodInformation.getName();
	}

	public int getParameterCount()
	{
		return this.methodInformation.getParameterCount();
	}

	public boolean isPublic()
	{
		return Modifier.isPublic(this.methodInformation.getModifiers());
	}

	public boolean isPrivate()
	{
		return Modifier.isPrivate(this.methodInformation.getModifiers());
	}

	public boolean isStatic()
	{
		return Modifier.isStatic(this.methodInformation.getModifiers());
	}

	public boolean isFinal()
	{
		return Modifier.isFinal(this.methodInformation.getModifiers());
	}

	public boolean isProtected()
	{
		return Modifier.isProtected(this.methodInformation.getModifiers());
	}
	
}
