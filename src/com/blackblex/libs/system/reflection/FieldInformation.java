/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.reflection
 *
 * ==============Information==============
 *      Filename: FieldInformation.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class FieldInformation
{

	private Object objectInformation;
	private Field fieldInformation;
	private Object valueField = null;

	public FieldInformation(Field field, Object obj)
	{
		this.fieldInformation = field;
		this.objectInformation = obj;
	}

	public String getName()
	{
		return this.fieldInformation.getName();
	}

	public String getType()
	{
		return this.fieldInformation.getType().getSimpleName();
	}
	
	public void setAccessible(boolean flag)
	{
		this.fieldInformation.setAccessible(flag);
	}
	
	public void setValue(Object value) throws IllegalArgumentException, IllegalAccessException
	{
		this.fieldInformation.set(this.objectInformation, value);
		this.valueField = null;
	}

	public Object getValue() throws IllegalArgumentException, IllegalAccessException
	{
		if ( this.valueField == null )
			this.valueField = this.fieldInformation.get(this.objectInformation);
		return this.valueField;
	}
	
	public void setValueInt(int value) throws IllegalArgumentException, IllegalAccessException
	{
		this.fieldInformation.setInt(this.objectInformation, value);
		this.valueField = null;
	}

	public int getValueToInt() throws IllegalArgumentException, IllegalAccessException
	{
		if ( this.valueField == null )
			getValue();

		return Integer.parseInt(this.valueField.toString());
	}
	
	public void setValueLong(Long value) throws IllegalArgumentException, IllegalAccessException
	{
		this.fieldInformation.setLong(this.objectInformation, value);
		this.valueField = null;
	}

	public long getValueToLong() throws IllegalArgumentException, IllegalAccessException
	{
		if ( this.valueField == null )
			getValue();

		return Long.parseLong(this.valueField.toString());
	}
	
	public void setValueDouble(Double value) throws IllegalArgumentException, IllegalAccessException
	{
		this.fieldInformation.setDouble(this.objectInformation, value);
		this.valueField = null;
	}

	public double getValueToDouble() throws IllegalArgumentException, IllegalAccessException
	{
		if ( this.valueField == null )
			getValue();

		return Double.parseDouble(this.valueField.toString());
	}
	
	public void setValueFloat(Float value) throws IllegalArgumentException, IllegalAccessException
	{
		this.fieldInformation.setFloat(this.objectInformation, value);
		this.valueField = null;
	}

	public float getValueToFloat() throws IllegalArgumentException, IllegalAccessException
	{
		if ( this.valueField == null )
			getValue();

		return Float.parseFloat(this.valueField.toString());
	}

	public boolean isPublic()
	{
		return Modifier.isPublic(this.fieldInformation.getModifiers());
	}

	public boolean isPrivate()
	{
		return Modifier.isPrivate(this.fieldInformation.getModifiers());
	}

	public boolean isStatic()
	{
		return Modifier.isStatic(this.fieldInformation.getModifiers());
	}

	public boolean isFinal()
	{
		return Modifier.isFinal(this.fieldInformation.getModifiers());
	}

	public boolean isProtected()
	{
		return Modifier.isProtected(this.fieldInformation.getModifiers());
	}

	@Override
	public String toString()
	{
		String split = " - ", is = "", result = "";

		if ( this.isPublic() )
			is += "public ";
		if ( this.isPrivate() )
			is += "private ";
		if ( this.isStatic() )
			is += "static ";
		if ( this.isFinal() )
			is += "final ";
		if ( this.isProtected() )
			is += "protected ";

		result = "Name: " + this.getName() + split + "Type: " + this.getType() + split + "Modifiers: " + is + "- ";

		try
		{
			if ( this.isPrivate() )
				this.fieldInformation.setAccessible(true);
			result += "Value: " + this.getValue();

			if ( this.isPrivate() )
			{
				result += " | ForcedAccess ";
				this.fieldInformation.setAccessible(false);
			}
		}
		catch ( IllegalArgumentException | IllegalAccessException e )
		{
			e.printStackTrace();
		}
		return result;
	}
}
