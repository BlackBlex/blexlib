/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.sql
 *
 * ==============Information==============
 *      Filename: ConnectionBD.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.blackblex.libs.core.file.FileProperties;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.utils.SystemVariables;

class ConnectionBD
{

	private String db_user = "", db_pass = "", db_name = "", db_host = "", server = "",
			driver = "com.mysql.jdbc.Driver";

	private SystemVariables systemVariables;
	private FileProperties config;

	private Connection connection = null;

	public ConnectionBD()
	{
		this.systemVariables = new SystemVariables();
		this.config = new FileProperties(
				this.systemVariables.getPath() + this.systemVariables.getDirectorySeparator() + "db.cfg");
		this.db_user = this.config.getString("db_user");
		this.db_pass = this.config.getString("db_pass");
		this.db_name = this.config.getString("db_name");
		this.db_host = this.config.getString("db_host");
		this.server = "jdbc:mysql://" + this.db_host + "/" + this.db_name;
	}

	public Connection getConnection()
	{
		if ( this.connection == null )
		{
			try
			{
				Class.forName(this.driver);
				this.connection = DriverManager.getConnection(this.server, this.db_user, this.db_pass);
				return this.connection;
			}
			catch ( ClassNotFoundException | SQLException e )
			{
				Init.MSG.printError(e);
				this.connection = null;
				return this.connection;
			}
		}
		else
		{
			return this.connection;
		}
	}

}
