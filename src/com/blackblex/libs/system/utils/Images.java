/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Images.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.awt.Dimension;
import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Images
{

	public Images()
	{
	}

	public static Icon getImagen(String path, JLabel comp, Dimension dim)
	{
		SystemVariables systemVariable = new SystemVariables();
		ImageIcon icon = new ImageIcon(systemVariable.getPath() + path);
		comp.setSize(dim);
		Icon icono = new ImageIcon(
				icon.getImage().getScaledInstance(comp.getWidth(), comp.getHeight(), Image.SCALE_SMOOTH));
		return icono;
	}

	public static Icon getImagenL(Class classLoad, String path, JLabel comp, Dimension dim)
	{
		ImageIcon icon = new ImageIcon(classLoad.getClass().getResource(path));
		comp.setSize(dim);
		Icon icono = new ImageIcon(
				icon.getImage().getScaledInstance(comp.getWidth(), comp.getHeight(), Image.SCALE_SMOOTH));
		return icono;
	}
}
