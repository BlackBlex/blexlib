/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Version.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.io.Serializable;
import java.util.StringTokenizer;

import com.blackblex.libs.core.exceptions.VersionFormatException;

public class Version implements Comparable <Version>, Cloneable, Serializable
{

	private static final long serialVersionUID = -250344051021951496L;
	private int major;
	private int minor;
	private int build;
	private int revision;
	private Status status;
	
	public Version(String version) throws VersionFormatException
	{
		if ( !version.contains(".")  )
			throw new VersionFormatException("For version string: " + version);
		
		StringTokenizer versionValues = new StringTokenizer(version, ".");
		
		this.major = Integer.parseInt(versionValues.nextToken());

		if ( versionValues.hasMoreTokens() )
			this.minor = Integer.parseInt(versionValues.nextToken());
		
		if ( versionValues.hasMoreTokens() )
			this.build = Integer.parseInt(versionValues.nextToken());
		
		if ( versionValues.hasMoreTokens() )
			this.revision = Integer.parseInt(versionValues.nextToken());

		this.status = Status.NONE;
	}

	public Version(int major)
	{
		this.major = major;
		this.minor = 0;
		this.build = 0;
		this.revision = 0;
		this.status = Status.NONE;
	}

	public Version(int major, int minor)
	{
		this.major = major;
		this.minor = minor;
		this.build = 0;
		this.revision = 0;
		this.status = Status.NONE;
	}

	public Version(int major, int minor, int build)
	{
		this.major = major;
		this.minor = minor;
		this.build = build;
		this.revision = 0;
		this.status = Status.NONE;
	}

	public Version(int major, int minor, int build, int revision)
	{
		this.major = major;
		this.minor = minor;
		this.build = build;
		this.revision = revision;
		this.status = Status.NONE;
	}

	public Version(int major, int minor, int build, int revision, Status status)
	{
		this.major = major;
		this.minor = minor;
		this.build = build;
		this.revision = revision;
		this.status = status;
	}

	public Version clone()
	{
		return new Version(this.major, this.minor, this.build, this.revision, this.status);
	}
	
	public boolean isGreaterThan(Version compare)
	{
		return (compareTo(compare) == 1);
	}
	
	public boolean isLessThan(Version compare)
	{
		return (compareTo(compare) == -1);
	}
	
	public boolean isEquality(Version compare)
	{
		return (compareTo(compare) == 0);
	}
	
	@Override
	public int compareTo(Version other)
	{
		int result = 0;

		if ( this.major < other.major )
			result = -1;
		else if ( this.major > other.major )
			result = 1;
		else
			result = 0;

		if ( result == 0 )
		{
			if ( this.minor < other.minor )
				result = -1;
			else if ( this.minor > other.minor )
				result = 1;
			else
				result = 0;
		}

		if ( result == 0 )
		{
			if ( this.build < other.build )
				result = -1;
			else if ( this.build > other.build )
				result = 1;
			else
				result = 0;
		}

		if ( result == 0 )
		{
			if ( this.revision < other.revision )
				result = -1;
			else if ( this.revision > other.revision )
				result = 1;
			else
				result = 0;
		}

		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if ( obj == null )
			return false;
		if ( this == obj )
			return true;
		if ( !(obj instanceof Version) )
			return false;
		Version v = (Version) obj;
		return (this.compareTo(v) == 0);

	}

	@Override
	public String toString()
	{
		StringBuilder version = new StringBuilder();

		version.append(this.major);
		version.append(".");
		version.append(this.minor);
		version.append(".");
		version.append(this.build);
		version.append(".");
		version.append(this.revision);
		if ( !this.status.getStatus().isEmpty() )
			version.append(".");
		version.append(this.status.getStatus());

		return version.toString();
	}
	
	public int getMajor()
	{
		return this.major;
	}

	public void setMajor(int major)
	{
		this.major = major;
	}

	public int getMinor()
	{
		return this.minor;
	}

	public void setMinor(int minor)
	{
		this.minor = minor;
	}

	public int getBuild()
	{
		return this.build;
	}

	public void setBuild(int build)
	{
		this.build = build;
	}

	public int getRevision()
	{
		return this.revision;
	}

	public void setRevision(int revision)
	{
		this.revision = revision;
	}

	public Status getStatus()
	{
		return this.status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}
	
	public enum Status
	{
		ALPHA("A"),
		BETA("B"),
		RELEASE_CANDITE("RC"),
		FINAL("F"),
		NONE("");

		private String letter;

		private Status(String letter)
		{
			this.letter = letter;
		}

		public String getStatus()
		{
			return this.letter;
		}
	}
}
