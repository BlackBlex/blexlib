/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils.table
 *
 * ==============Information==============
 *      Filename: jMessageModel.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils.table;

import java.util.List;
import javax.swing.table.AbstractTableModel;

import com.blackblex.libs.net.objects.jMessage;

public class jMessageModel extends AbstractTableModel
{

	private static final long serialVersionUID = 2966577023966386181L;
	List items;

	public jMessageModel(List item)
	{
		this.items = item;
	}

	@Override
	public Class getColumnClass(int columnIndex)
	{
		return jMessage.class;
	}

	@Override
	public int getColumnCount()
	{
		return 1;
	}

	@Override
	public String getColumnName(int columnIndex)
	{
		return " ";
	}

	@Override
	public int getRowCount()
	{
		return (this.items == null) ? 0 : items.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		if ( rowIndex >= this.items.size() )
			rowIndex--;
		return (this.items == null) ? null : this.items.get(rowIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
		return false;
	}

	public void removeRow(int row)
	{
		items.remove(row);
		fireTableRowsDeleted(row, row);
	}
}
