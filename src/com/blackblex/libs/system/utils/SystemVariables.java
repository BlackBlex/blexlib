/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: SystemVariables.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class SystemVariables
{

	private Map <String, String> SYSTEM_PROPERTIES = new HashMap <>();
	private Properties systemProperties = System.getProperties();
	private String LINE_SEPARATOR, PATH_SEPARATOR, DIRECTORY_SEPARATOR, USER;

	public SystemVariables()
	{
		initProperties();
	}

	private void initProperties()
	{
		for ( Entry <Object, Object> x : this.systemProperties.entrySet() )
		{
			this.SYSTEM_PROPERTIES.put((String) x.getKey(), (String) x.getValue());
		}

		// Set System variables
		this.setLineSeparator(this.SYSTEM_PROPERTIES.get("line.separator"));
		this.setPathSeparator(this.SYSTEM_PROPERTIES.get("path.separator"));
		this.setDirectorySeparator(this.SYSTEM_PROPERTIES.get("file.separator"));
		this.setUser(this.SYSTEM_PROPERTIES.get("user.name"));
	}

	public String getDirectorySeparator()
	{
		return this.DIRECTORY_SEPARATOR;
	}

	public String getLineSeparator()
	{
		return this.LINE_SEPARATOR;
	}

	public String getPathSeparator()
	{
		return this.PATH_SEPARATOR;
	}

	public String getPath()
	{
		return this.SYSTEM_PROPERTIES.get("user.dir");
	}

	public String getPath(String dir)
	{
		String path = null;
		switch ( dir )
		{
			case "home":
				path = this.SYSTEM_PROPERTIES.get("user.home");
				break;
			case "user":
				path = this.SYSTEM_PROPERTIES.get("user.name");
				break;
			case "desktop":
				path = this.SYSTEM_PROPERTIES.get("user.home") + this.DIRECTORY_SEPARATOR + "Desktop";
				break;

		}
		return path;
	}

	public String getUser()
	{
		return this.USER;
	}

	private void setDirectorySeparator(String directory)
	{
		this.DIRECTORY_SEPARATOR = directory;
	}

	private void setLineSeparator(String line)
	{
		this.LINE_SEPARATOR = line;
	}

	private void setPathSeparator(String path)
	{
		this.PATH_SEPARATOR = path;
	}

	private void setUser(String user)
	{
		this.USER = user;
	}
}
