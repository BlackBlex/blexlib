/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Messages.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import javax.swing.JOptionPane;

public class Messages
{
	private String info = ((char) 161) + "Informaci" + ((char) 243)  + "n del sistema" + ((char) 33),
				  error = ((char) 161) + "Error del sistema" + ((char) 33),
			    warning = ((char) 161) + "Precauci" + ((char) 243) + "n del sistema" + ((char) 33);

	public Messages()
	{
		System.out.println((char)161);
	}

	public void showMessageInformation(String msg)
	{
		showMessage(msg, this.info, JOptionPane.INFORMATION_MESSAGE);
	}

	public void showMessageError(String msg)
	{
		showMessage(msg, this.error, JOptionPane.ERROR_MESSAGE);
	}

	public void showMessageWarning(String msg)
	{
		showMessage(msg, this.warning, JOptionPane.WARNING_MESSAGE);
	}

	private void showMessage(String msg, String title, int option)
	{
		JOptionPane.showMessageDialog(null, msg, title, option);
	}

	public boolean showConfirmInformation(String msg)
	{
		return showConfimDDialog(msg, this.info);
	}

	public boolean showConfirmError(String msg)
	{
		return showConfimDDialog(msg, this.error);
	}

	public boolean showConfirmWarning(String msg)
	{
		return showConfimDDialog(msg, this.warning);
	}

	private boolean showConfimDDialog(String msg, String title)
	{
		int option = JOptionPane.showConfirmDialog(null, msg, title, JOptionPane.YES_NO_OPTION);
		return option == JOptionPane.YES_OPTION;
	}

	public int getInt(String msg)
	{
		int result = 0;

		try
		{
			result = Integer.parseInt(getInput(msg));
		}
		catch ( NumberFormatException ex )
		{

		}

		return result;
	}

	public double getDouble(String msg)
	{
		double result = 0;

		try
		{
			result = Double.parseDouble(getInput(msg));
		}
		catch ( NumberFormatException ex )
		{

		}

		return result;
	}

	public String getString(String msg)
	{
		return getInput(msg);
	}

	private String getInput(String msg)
	{
		String result = JOptionPane.showInputDialog(null, msg, "Ingrese el valor", JOptionPane.INFORMATION_MESSAGE);
		return result;
	}

	public void printMsg(String msg)
	{
		System.out.println("**********************************************\n" + " Mensaje del sistema: \n" + "  " + msg
				+ "\n" + "**********************************************\n");
	}

	public void printError(Exception e)
	{
		StackTraceElement elements[] = e.getStackTrace();
		for ( StackTraceElement element : elements )
		{

			if ( element.getLineNumber() > -1 )
			{
				System.out.println("**********************************************\n" + "           Error: "
						+ e.getClass() + "\n" + "      Localizado: " + e.getLocalizedMessage() + "\n"
						+ " Fue causado por: " + e.getCause() + "\n" + "           Linea: " + element.getLineNumber()
						+ "\n" + "           Clase: " + element.getClassName() + "\n" + "         Archivo: "
						+ element.getFileName() + "\n" + "          Metodo: " + element.getMethodName() + "\n"
						+ "**********************************************\n");
			}
		}

	}
}