/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Sorting.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

public class Sorting
{

	private static int arrayP[] = null;

	public static void bubble(int array[])
	{
		int i, j, aux;
		for ( i = 0; i < array.length; i++ )
		{
			for ( j = 0; j < array.length - 1; j++ )
			{
				if ( array[j] > array[j + 1] ) // < o >
				{
					aux = array[j];
					array[j] = array[j + 1];
					array[j + 1] = aux;
				}
			}
		}
		arrayP = array;
	}

	public static void print()
	{
		if ( null == arrayP )
		{
			System.out.println("Primero debes de usar algun metodo de sorting");
			return;
		}
		int i;
		for ( i = 0; i < arrayP.length; i++ )
		{
			System.out.print(arrayP[i] + " ");
		}
		System.out.println();
	}

	public static int [] getArray()
	{
		if ( null == arrayP )
		{
			System.out.println("Primero debes de usar algun metodo de sorting");
			return null;
		}
		return arrayP;
	}

}
