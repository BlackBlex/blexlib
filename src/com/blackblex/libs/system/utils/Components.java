/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Components.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JFrame;
import javax.swing.JLabel;
import com.blackblex.libs.core.file.FileProperties;

public class Components
{

	private String jf;
	private SystemVariables v;
	public FileProperties theme, language, config;

	public Components(String jf)
	{
		this.jf = jf;

		v = new SystemVariables();
		config = new FileProperties(v.getPath() + v.getDirectorySeparator() + "settings.cfg");

		theme = new FileProperties(v.getPath() + v.getDirectorySeparator() + "theme.cfg");

		language = new FileProperties(v.getPath() + v.getDirectorySeparator() + "languages" + v.getDirectorySeparator()
				+ config.getString("language") + ".lang");
	}

	public void setConfig(Component obj, String text)
	{
		obj.setName(text);

		try
		{
			if ( obj.getClass().getSimpleName().contains("JLabel") )
			{
				JLabel jlabel = (JLabel) obj;
				jlabel.setText(language.getString(this.jf + "_" + obj.getName()));
			}
			if ( obj.getClass().getSimpleName().contains("JFrame") )
			{
				JFrame jframe = (JFrame) obj;
				jframe.setTitle(language.getString(this.jf + "_" + obj.getName()));
			}
		}
		catch ( NumberFormatException e )
		{
			System.out.println(obj.getClass().getName());

		}

		try
		{
			obj.setForeground(Color.decode("#" + theme.getString(obj.getName() + "fontcolor")));
		}
		catch ( NumberFormatException e )
		{
			obj.setForeground(
					Color.decode("#" + theme.getString(obj.getClass().getSimpleName().toLowerCase() + "fontcolor")));
		}

		try
		{
			obj.setBackground(Color.decode("#" + theme.getString(obj.getName() + "background")));
		}
		catch ( NumberFormatException e )
		{
			obj.setBackground(
					Color.decode("#" + theme.getString(obj.getClass().getSimpleName().toLowerCase() + "background")));
		}

	}

	public void setName(Component obj, String text)
	{
		obj.setName(jf.getClass().getSimpleName() + "_" + text);
	}

}
