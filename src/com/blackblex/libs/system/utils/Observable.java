/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Observable.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.util.ArrayList;

public abstract class Observable
{

	private static ArrayList <Observer> observers = new ArrayList <Observer>();
	private ArrayList <Object> args = new ArrayList <Object>();

	public void add(Observer obs)
	{
		observers.add(obs);
	}

	public void remove(Observer obs)
	{
		observers.remove(obs);
	}

	public void addArgs(Object arg)
	{
		args.add(arg);
	}

	public Object getArgs(int index)
	{
		return args.get(index);
	}

	public void removeArgs(Object arg)
	{
		args.remove(arg);
	}

	public void notifyObs()
	{

		for ( Observer obs : observers )
		{
			obs.update(this);
		}

	}

	public abstract void action();

	public String actionString()
	{
		return "";
	}

	public int actionInt()
	{
		return 0;
	}

	public Object actionObject()
	{
		return new Object();
	}

}
