/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: MultiType.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.util.ArrayList;
import java.util.Arrays;

public class MultiType<T>
{

	private ArrayList <T> value = new ArrayList <>();

	public MultiType()
	{

	}

	public MultiType(T... items)
	{
		this.value.addAll(Arrays.asList(items));
	}

	private T get(int pos)
	{
		return this.value.get(pos);
	}

	public ArrayList <T> getAll()
	{
		return this.value;
	}

	private void set(T item)
	{
		this.value.add(item);
	}

}
