/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.timer
 *
 * ==============Information==============
 *      Filename: Display.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.timer;

class Display
{

	void printTime(int hour, int minute, int second)
	{
		String fullHour = "";

		fullHour += (hour > 9) ? ":" + hour : "0" + hour;
		fullHour += (minute > 9) ? ":" + minute : ":0" + minute;
		fullHour += (second > 9) ? ":" + second : ":0" + second;

		System.out.println(fullHour);
	}
}
