/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.timer
 *
 * ==============Information==============
 *      Filename: TimerDisplay.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.timer;

import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class TimerDisplay extends JLabel
{

	private static final long serialVersionUID = -3245659612377695494L;
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

	public TimerDisplay(Observable modelo)
	{

		this.setHorizontalAlignment((SwingConstants.CENTER));

		modelo.addObserver(new Observer()
		{

			@Override
			public void update(java.util.Observable o, Object arg)
			{
				final Object fecha = arg;

				SwingUtilities.invokeLater(new Runnable()
				{

					@Override
					public void run()
					{
						setText(format.format(fecha));
					}
				});
			}
		});

		this.setPreferredSize(new Dimension(200, 50));
	}

	public void setFormat(SimpleDateFormat unFormato)
	{
		format = unFormato;
	}

}
