/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.datetime
 *
 * ==============Information==============
 *      Filename: Date.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.datetime;

import java.text.SimpleDateFormat;

public class Date
{

	private String format;

	public Date()
	{
		this.format = "dd-MM-yyyy";
	}

	public Date(String format)
	{
		this.format = format;
	}

	public String getDate()
	{
		SimpleDateFormat formatter = new SimpleDateFormat(this.format);
		String temp = formatter.format(new Date());
		return temp;
	}

}
