/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.components.borders
 *
 * ==============Information==============
 *      Filename: CircleBorder.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.components.borders;

import java.awt.Color;
import java.awt.BasicStroke;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import javax.swing.border.AbstractBorder;

public class CircleBorder extends AbstractBorder
{

	private static final long serialVersionUID = 1796121429444944587L;
	private Color color;
	private BasicStroke stroke = null;
	private RenderingHints hints;

	public CircleBorder()
	{
		this.color = Color.BLACK;
		this.stroke = new BasicStroke(1);
		this.hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	public CircleBorder(Color color, int stroke)
	{
		this.color = color;
		this.stroke = new BasicStroke(stroke);
		this.hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	@Override
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height)
	{

		Graphics2D g2 = (Graphics2D) g;
		Ellipse2D circle2D = new Ellipse2D.Double();

		if ( this.stroke.getLineWidth() == 0 )
		{
			circle2D.setFrameFromCenter(new Point(x + width / 2, y + height / 2), new Point(width, height));
		}
		else
		{
			circle2D.setFrameFromCenter(new Point(x + width / 2, y + height / 2),
					new Point(width - (int) this.stroke.getLineWidth(), height - (int) this.stroke.getLineWidth()));
		}

		Polygon pointer = new Polygon();
		Area area = new Area(circle2D);
		area.add(new Area(pointer));
		g2.setRenderingHints(this.hints);

		Component parent = c.getParent();
		if ( parent != null )
		{
			Color bg = parent.getBackground();
			Rectangle rect = new Rectangle(0, 0, width, height);
			Area borderRegion = new Area(rect);
			borderRegion.subtract(area);
			g2.setClip(borderRegion);
			g2.setColor(bg);
			g2.fillRect(0, 0, width, height);
			g2.setClip(null);
		}

		if ( this.stroke.getLineWidth() > 0 )
		{
			g2.setColor(this.color);
			g2.setStroke(this.stroke);
		}

		g2.draw(area);
	}
}
