/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.core.interfaces
 *
 * ==============Information==============
 *      Filename: IPluginData.java
 * ---------------------------------------
*/

package com.blackblex.plugins.core.interfaces;

import java.util.ArrayList;

public class IPluginData
{

	private ArrayList <String> pluginRequiered = new ArrayList <>();

	public void addRequiered(String plugin)
	{
		this.pluginRequiered.add(plugin);
	}

	public ArrayList <String> getRequiered()
	{
		return this.pluginRequiered;
	}

}
