/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.core.interfaces
 *
 * ==============Information==============
 *      Filename: IPluginInfo.java
 * ---------------------------------------
*/

package com.blackblex.plugins.core.interfaces;

import com.blackblex.libs.system.utils.Version;
import com.blackblex.plugins.core.Core;

public interface IPluginInfo
{

	String getName();

	String getAuthor();

	String getDescription();

	Core.TYPE getType();

	Version getVersion();

}
