/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.core
 *
 * ==============Information==============
 *      Filename: PluginCore.java
 * ---------------------------------------
*/

package com.blackblex.plugins.core;

import com.blackblex.libs.system.utils.Observer;
import com.blackblex.plugins.core.interfaces.IPlugin;
import com.blackblex.plugins.core.interfaces.IPluginData;
import com.blackblex.plugins.core.interfaces.IPluginInfo;
import java.awt.Component;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;

public abstract class PluginCore extends IPluginData implements IPlugin, IPluginInfo, Observer
{

	public List <Component> getAllComponents(final Container c)
	{
		Component [] comps = c.getComponents();
		List <Component> compList = new ArrayList <>();
		for ( Component comp : comps )
		{
			compList.add(comp);
			if ( comp instanceof Container )
			{
				compList.addAll(getAllComponents((Container) comp));
			}
		}
		return compList;
	}

	public void wait(int milisec)
	{
		try
		{
			Thread.sleep(milisec);
		}
		catch ( InterruptedException ex )
		{

		}
	}
}
