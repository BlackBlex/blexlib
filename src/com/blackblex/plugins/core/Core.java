/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.core
 *
 * ==============Information==============
 *      Filename: Core.java
 * ---------------------------------------
*/

package com.blackblex.plugins.core;

import com.blackblex.plugins.loaders.AvailablePlugins;
import com.blackblex.plugins.loaders.LoadPlugins;
import java.util.ArrayList;

public class Core
{

	public static GlobalService globalService = null;
	public static AvailablePlugins availableplugins = new AvailablePlugins();

	private ArrayList <PluginCore> lastExecute = new ArrayList <>();
	private boolean load;

	public static enum TYPE
	{

		CORE(1), MODULE(2), COMPLEMENT(3);

		private TYPE(int val)
		{
			this.value = val;
		}

		public int getValue()
		{
			return this.value;
		}

		private int value;
	};

	public Core(String name)
	{
		PluginCore [] plugins;

		this.load = LoadPlugins.loadPlugins(name);

		if ( this.load )
		{
			try
			{
				plugins = orderPlugin(LoadPlugins.getPlugins());

				if ( plugins.length > 0 )
				{
					for ( PluginCore a : plugins )
					{
						Core.availableplugins.add(a);
					}
				}
				else
				{
					Core.Message.printlnError("No se encontraron plugins");
				}
			}
			catch ( Exception ex )
			{
				System.err.println("Excepcion core: " + ex.getMessage());
			}
		}
		else
		{
			Core.Message.printlnError("Plugins error");
		}

	}

	public void setGlobalService(GlobalService global)
	{
		globalService = global;
	}

	public void exec()
	{
		execute();
		lastexecute();
	}

	private void execute()
	{
		if ( this.load )
		{
			for ( PluginCore a : Core.availableplugins.getPlugins() )
			{
				a.start();

				Core.Message.printlnStatus("INIT", "Loading: " + a.getName());

				int counter = 0;
				for ( String requiere : a.getRequiered() )
				{
					Core.Message.printlnStatus("INIT", "It requires: " + requiere);
					boolean pload = Core.availableplugins.isLoad(Core.availableplugins.getPlugin(requiere));
					if ( pload )
					{
						counter++;
					}
					else
					{
						Core.Message.printlnError(
								"Plugin: " + requiere + " not loaded, " + a.getName() + " will move to the last");
						this.lastExecute.add(a);
						break;
					}
				}

				if ( counter == a.getRequiered().size() )
				{
					Core.Message.printlnStatus("LOAD", "Running the load();");
					a.load();
					Core.availableplugins.setStatus(a, true);
				}

				System.out.println();
			}
		}
	}

	private void lastexecute()
	{
		if ( this.lastExecute.size() > 0 )
		{
			Core.Message.printlnStatus("ALERT", "Last execution");
			for ( PluginCore a : this.lastExecute )
			{
				Core.Message.printlnStatus("INIT", "Loading: " + a.getName());
				int counter = 0;
				for ( String requiere : a.getRequiered() )
				{
					Core.Message.printlnStatus("INIT", "It requires: " + requiere);
					boolean pload = Core.availableplugins.isLoad(Core.availableplugins.getPlugin(requiere));
					if ( pload )
					{
						counter++;
					}
					else
					{
						Core.Message.printlnError(
								"Plugin: " + requiere + " not loaded, " + a.getName() + " execution is discarded.");
						counter = 0;
						break;
					}
				}

				if ( counter == a.getRequiered().size() )
				{
					Core.Message.printlnStatus("LOAD", "Running the load();");
					a.load();
					Core.availableplugins.setStatus(a, true);
				}

				this.lastExecute = null;
				System.out.println();
			}
		}
	}

	public boolean closePlugins()
	{
		boolean status = false;

		Core.Message.printlnStatus("ALERT", "Unloading plugins");

		for ( PluginCore a : Core.availableplugins.getPlugins() )
		{
			Core.Message.printlnStatus("UNLOAD", a.getName());
			if ( a.end() )
				Core.availableplugins.remove(a);
		}

		if ( Core.availableplugins.getPlugins().length == 0 )
			status = true;

		return status;
	}

	private PluginCore [] orderPlugin(PluginCore [] pl)
	{
		ArrayList <PluginCore> rpl = new ArrayList <>();
		for ( PluginCore p : pl )
			if ( p.getType() == TYPE.CORE )
				rpl.add(p);
		for ( PluginCore p : pl )
			if ( p.getType() == TYPE.MODULE )
				rpl.add(p);
		for ( PluginCore p : pl )
			if ( p.getType() == TYPE.COMPLEMENT )
				rpl.add(p);

		return rpl.toArray(new PluginCore [0]);
	}

	public static class Message
	{
		public static void printError(String text)
		{
			printText("[ERROR] " + text);
		}

		public static void printlnError(String text)
		{
			printText("[ERROR] " + text + "" + System.getProperty("line.separator"));
		}

		public static void printStatus(String status, String text)
		{
			printText("[" + status + "] " + text);
		}

		public static void printlnStatus(String status, String text)
		{
			printText("[" + status + "] " + text + "" + System.getProperty("line.separator"));
		}

		public static void print(String text)
		{
			printText(text);
		}

		public static void println(String text)
		{
			printText(text + "" + System.getProperty("line.separator"));
		}

		private static void printText(String text)
		{
			System.out.print(text);
		}
	}

}
