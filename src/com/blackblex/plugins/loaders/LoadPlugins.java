/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.loaders
 *
 * ==============Information==============
 *      Filename: LoadPlugins.java
 * ---------------------------------------
*/

package com.blackblex.plugins.loaders;

import com.blackblex.plugins.core.PluginCore;
import com.blackblex.plugins.modificators.ModifierClassPath;
import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ServiceLoader;

public class LoadPlugins
{

	private static final String EXTENSION_JAR = ".jar";
	private static String NAME_START_JAR = "";
	private static final String PLUGINS_DIRECTORY = "plugins/";

	public static boolean loadPlugins(String name)
	{
		NAME_START_JAR = name;
		boolean loads = true;
		try
		{

			File [] jars = searchPlugins();

			if ( jars.length > 0 )
			{
				ModifierClassPath cp = new ModifierClassPath();

				for ( File jar : jars )
				{
					try
					{
						cp.addFile(jar);
					}
					catch ( MalformedURLException ex )
					{
						System.err.println("URL incorrecta: " + ex.getMessage());
					}
				}
			}
		}
		catch ( NoSuchMethodException ex )
		{
			loads = false;
			System.err.println(ex.getMessage());
		}
		return loads;
	}

	private static File [] searchPlugins()
	{
		ArrayList <File> vUrls = new ArrayList <>();

		File pluginsDirectory = new File(PLUGINS_DIRECTORY);

		if ( pluginsDirectory.exists() && pluginsDirectory.isDirectory() )
		{

			File [] jars = pluginsDirectory.listFiles(new FilenameFilter()
			{

				@Override
				public boolean accept(File dir, String name)
				{
					return name.toLowerCase().startsWith(NAME_START_JAR.toLowerCase()) && name.endsWith(EXTENSION_JAR);
				}
			});

			vUrls.addAll(Arrays.asList(jars));
		}

		return vUrls.toArray(new File [0]);
	}

	public static PluginCore [] getPlugins()
	{

		ServiceLoader <PluginCore> sl = ServiceLoader.load(PluginCore.class);
		sl.reload();

		ArrayList <PluginCore> vAv = new ArrayList <>();

		for ( PluginCore pl : sl )
		{
			try
			{
				vAv.add(pl);
			}
			catch ( Exception ex )
			{
				System.err.println("Excepcion al obtener plugin: " + ex.getMessage());
			}
		}

		return vAv.toArray(new PluginCore [0]);
	}

}
