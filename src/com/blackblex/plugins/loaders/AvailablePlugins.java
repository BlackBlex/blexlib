/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.loaders
 *
 * ==============Information==============
 *      Filename: AvailablePlugins.java
 * ---------------------------------------
*/

package com.blackblex.plugins.loaders;

import com.blackblex.plugins.core.PluginCore;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class AvailablePlugins
{
	private Map <PluginCore, Boolean> plugins = new LinkedHashMap <>();

	public AvailablePlugins()
	{

	}

	public void add(PluginCore p)
	{
		this.plugins.put(p, false);
	}

	public void remove(PluginCore p)
	{
		this.plugins.remove(p);
	}

	public void setStatus(PluginCore p, boolean b)
	{
		this.plugins.put(p, b);
	}

	public boolean isLoad(PluginCore p)
	{
		try
		{
			return this.plugins.get(p);
		}
		catch ( java.lang.NullPointerException ex )
		{
			return false;
		}
	}

	public PluginCore [] getPlugins()
	{
		ArrayList <PluginCore> rpl = new ArrayList <>();

		for ( PluginCore p : this.plugins.keySet() )
			rpl.add(p);

		return rpl.toArray(new PluginCore [0]);
	}

	public PluginCore getPlugin(String name)
	{
		PluginCore result = null;
		for ( Map.Entry <PluginCore, Boolean> p : this.plugins.entrySet() )
			if ( p.getKey().getName().contains(name) )
				return p.getKey();

		return result;
	}

}