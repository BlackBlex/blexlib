/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.modificators
 *
 * ==============Information==============
 *      Filename: ModifierClassPath.java
 * ---------------------------------------
*/

package com.blackblex.plugins.modificators;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class ModifierClassPath
{

	private static final String ADD_METHOD = "addURL";
	private static final Class [] PARS_METHOD = new Class [] {URL.class };
	private final URLClassLoader loader;
	private final Method addMethod;

	public ModifierClassPath() throws NoSuchMethodException
	{
		loader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		addMethod = URLClassLoader.class.getDeclaredMethod(ADD_METHOD, PARS_METHOD);
		addMethod.setAccessible(true);
	}

	public URL [] getURLs()
	{
		return loader.getURLs();
	}

	public void addURL(URL url)
	{
		if ( url != null )
		{
			try
			{
				addMethod.invoke(loader, new Object [] {url });
			}
			catch ( Exception ex )
			{
				System.err.println("Excepcion al guardar URL: " + ex.getLocalizedMessage());
			}
		}
	}

	public void addURLs(URL [] urls)
	{
		if ( urls != null )
		{
			for ( URL url : urls )
			{
				addURL(url);
			}
		}
	}

	public void addFile(File file) throws MalformedURLException
	{
		if ( file != null )
		{
			addURL(file.toURI().toURL());
		}
	}

	public void addFile(String fileName) throws MalformedURLException
	{
		addFile(new File(fileName));
	}
}
