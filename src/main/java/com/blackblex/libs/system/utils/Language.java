/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Language.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import com.blackblex.libs.core.file.FileProperties;

public class Language
{

	public FileProperties language;

	public Language(String lang)
	{
		init(lang);
	}

	public void init(String lang)
	{
		SystemVariables systemVariables = new SystemVariables();
		this.language = new FileProperties(systemVariables.getPath() + systemVariables.getDirectorySeparator()
				+ "languages" + systemVariables.getDirectorySeparator() + lang + ".lang");
	}

	public void setText(JMenu obj)
	{
		obj.setText(this.language.getString(obj.getName()));
	}

	public void setText(JFrame obj)
	{
		obj.setTitle(this.language.getString(obj.getName()));
	}

	public void setText(JMenuItem obj)
	{
		obj.setText(this.language.getString(obj.getName()));
	}

	public void setText(JButton obj)
	{
		obj.setText(this.language.getString(obj.getName()));
	}

	public void setText(JLabel obj)
	{
		obj.setText(this.language.getString(obj.getName()));
	}
}
