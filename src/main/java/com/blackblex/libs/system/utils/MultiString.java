/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: MultiString.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

public class MultiString
{

	private String one = null, two = null, three = null, four = null, five = null, six = null, seven = null,
			eight = null, nine = null, teen = null, eleven = null, twelve = null, thirteen = null, fourteen = null,
			fifteen = null;

	public MultiString()
	{
	}

	public MultiString(String o, String t)
	{
		one = o;
		two = t;
	}

	public MultiString(String o, String t, String th)
	{
		one = o;
		two = t;
		three = th;
	}

	public MultiString(String o, String t, String... more)
	{
		one = o;
		two = t;
		for ( int a = 3; a <= more.length - 1; a++ )
		{
			set(a, more[a]);
		}
	}

	public String getOne()
	{
		return one;
	}

	public String getTwo()
	{
		return two;
	}

	public String getThree()
	{
		return three;
	}

	public String getFour()
	{
		return four;
	}

	public String getFive()
	{
		return five;
	}

	public String getSix()
	{
		return six;
	}

	public String getSeven()
	{
		return seven;
	}

	public String getEight()
	{
		return eight;
	}

	public String getNine()
	{
		return nine;
	}

	public String getTeen()
	{
		return teen;
	}

	public String getEleven()
	{
		return eleven;
	}

	public String getTwelve()
	{
		return twelve;
	}

	public String getThirteen()
	{
		return thirteen;
	}

	public String getFourteen()
	{
		return fourteen;
	}

	public String getFifteen()
	{
		return fifteen;
	}

	public void set(int index, String s)
	{
		switch ( index )
		{
			case 1:
				setOne(s);
				break;
			case 2:
				setTwo(s);
				break;
			case 3:
				setThree(s);
				break;
			case 4:
				setFour(s);
				break;
			case 5:
				setFive(s);
				break;
			case 6:
				setSix(s);
				break;
			case 7:
				setSeven(s);
				break;
			case 8:
				setEight(s);
				break;
			case 9:
				setNine(s);
				break;
			case 10:
				setTeen(s);
				break;
			case 11:
				setEleven(s);
				break;
			case 12:
				setTwelve(s);
				break;
			case 13:
				setThirteen(s);
				break;
			case 14:
				setFourteen(s);
				break;
			case 15:
				setFifteen(s);
				break;
			default:
				break;
		}
	}

	public void setOne(String o)
	{
		one = o;
	}

	public void setTwo(String t)
	{
		two = t;
	}

	public void setThree(String th)
	{
		three = th;
	}

	public void setFour(String f)
	{
		four = f;
	}

	public void setFive(String fi)
	{
		five = fi;
	}

	public void setSix(String s)
	{
		six = s;
	}

	public void setSeven(String se)
	{
		seven = se;
	}

	public void setEight(String e)
	{
		eight = e;
	}

	public void setNine(String n)
	{
		nine = n;
	}

	public void setTeen(String t)
	{
		teen = t;
	}

	public void setEleven(String el)
	{
		eleven = el;
	}

	public void setTwelve(String tw)
	{
		twelve = tw;
	}

	public void setThirteen(String th)
	{
		thirteen = th;
	}

	public void setFourteen(String fot)
	{
		fourteen = fot;
	}

	public void setFifteen(String fit)
	{
		fifteen = fit;
	}
}
