/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Observer.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

public interface Observer
{

	void update(Observable obj);

}
