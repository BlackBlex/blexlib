/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils.table
 *
 * ==============Information==============
 *      Filename: TableCell.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.Map;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class TableCell<T> extends AbstractCellEditor implements TableCellEditor, TableCellRenderer
{

	private static final long serialVersionUID = 6304537635639772084L;
	JPanel panel;
	JLabel text;
	Map <String, String> args;
	String format = "";

	T item;

	public TableCell(Map <String, String> arg, String f)
	{
		this.args = arg;
		this.format = f;
		this.text = new JLabel();
		this.panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		this.panel.add(this.text);
	}

	private void updateData(T item, boolean isSelected, JTable table, int row)
	{
		this.item = item;
		String ready = this.format;

		for ( Map.Entry <String, String> val : this.args.entrySet() )
		{
			try
			{
				Method method = this.item.getClass().getMethod(val.getValue(), null);
				Object result = (Object) method.invoke(this.item, null);
				ready = ready.replace(val.getKey(), result.toString());
			}
			catch ( Exception ex )
			{
				ex.printStackTrace();
				Logger.getLogger(TableCell.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		this.text.setText(ready);

		if ( isSelected )
		{
			this.panel.setBackground(table.getSelectionBackground());
		}
		else
		{
			if ( row % 2 == 0 )
			{
				this.panel.setBackground(Color.decode("#F5F5F5"));
			}
			else
			{
				this.panel.setBackground(Color.WHITE);
			}
		}
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
	{
		T item = (T) value;
		updateData(item, true, table, row);
		return this.panel;
	}

	@Override
	public Object getCellEditorValue()
	{
		return null;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column)
	{
		T item = (T) value;
		updateData(item, isSelected, table, row);
		return this.panel;
	}
}
