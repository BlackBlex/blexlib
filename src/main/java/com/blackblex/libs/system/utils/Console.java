/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Console.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.concurrent.CountDownLatch;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.blackblex.libs.application.datetime.Date;
import com.blackblex.libs.application.datetime.Time;

public final class Console
{

	public final static GUI out = new GUI();

	private Console()
	{
	}

	public static class GUI
	{

		private JFrame consoleFrame;
		private JPanel consoleContainer;
		private Caret consoleContainerCaret;
		private JTextPane consoleContainerText;

		private StyledDocument consoleDocumentStyle;
		private SimpleAttributeSet consoleStyle;

		// Attributes
		private SystemVariables SYSTEM = new SystemVariables();
		private Date date = new Date("d/MMMM/yyyy");
		private Time time = new Time("h:mm a");
		private String newLine = SYSTEM.getLineSeparator();

		private String input;
		private String promptVal;

		private CountDownLatch latch;

		private int inputStart;
		private int lastInputStart;
		private int lastInputEnd;
		private int maxInput;

		private Color DEFAULT_COLOR_T;
		private Color DEFAULT_COLOR_TC;
		private Color DEFAULT_COLOR_B;
		private String DEFAULT_FONT;

		// Settings
		private int HEIGHT, WIDTH;
		private Color FONTCOLOR;
		private Color BACKGROUNDCOLOR;
		private boolean EDITABLE;
		private boolean SELECTED;
		private boolean BOLD;
		private String PATH_FONT;
		// private String PREFIX = "[LOGGER] (" + date.getDate() + " " +
		// time.getTime() + ") :";
		private String PREFIX;
		private String SUFFIX;

		private GUI()
		{
			init();
		}

		private void defaultSettingsStyles()
		{
			HEIGHT = 600;
			WIDTH = 300;
			DEFAULT_COLOR_T = Color.WHITE;
			DEFAULT_COLOR_TC = Color.WHITE;
			DEFAULT_COLOR_B = Color.BLACK;
			FONTCOLOR = DEFAULT_COLOR_T;
			BACKGROUNDCOLOR = DEFAULT_COLOR_B;
			maxInput = -1;
			EDITABLE = false;
			BOLD = false;
			PATH_FONT = "/fonts/Nunito-Regular.ttf";
			PREFIX = "";
			// PREFIX = "[LOGGER] (" + date.getDate() + " " + time.getTime() +
			// ") :";
			SUFFIX = "";
			DEFAULT_FONT = "Lucida Console";

			consoleContainerText.setBackground(DEFAULT_COLOR_B);
			consoleContainerText.setCaretColor(DEFAULT_COLOR_TC);
			consoleContainerText.setForeground(DEFAULT_COLOR_T);
			consoleContainerText.setFont(new Font(DEFAULT_FONT, Font.PLAIN, 13));

			consoleStyle = new SimpleAttributeSet();
			StyleConstants.setFontFamily(consoleStyle, DEFAULT_FONT);
			StyleConstants.setFontSize(consoleStyle, 12);
			StyleConstants.setForeground(consoleStyle, Color.WHITE);

		}

		private void init()
		{
			consoleFrame = new JFrame("Console");
			consoleContainer = new JPanel();
			consoleContainerText = new JTextPane();
			consoleDocumentStyle = consoleContainerText.getStyledDocument();
			defaultSettingsStyles();
			redirectSystemStreams();

			InputPolicy inputP = new InputPolicy();
			consoleContainerText.addKeyListener(inputP);
			consoleContainerText.setEditable(EDITABLE);
			consoleContainerText.setMargin(new Insets(10, 10, 10, 10));

			consoleContainerCaret = consoleContainerText.getCaret();
			consoleContainerCaret.setBlinkRate(250);
			consoleContainerCaret.addChangeListener(inputP);

			// BackSpace
			Action backspace = new AbstractAction()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if ( !EDITABLE )
					{
						return;
					}
					int dot = consoleContainerCaret.getDot();
					int mark = consoleContainerCaret.getMark();
					if ( dot < inputStart || mark < inputStart )
					{
						return;
					}
					if ( dot != mark )
					{
						int start = consoleContainerText.getSelectionStart();
						int end = consoleContainerText.getSelectionEnd();
						replaceRange("", start, end);
					}
					else if ( consoleContainerCaret.getDot() > inputStart )
					{
						replaceRange("", dot - 1, dot);
					}
				}
			};

			consoleContainerText.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0, false),
					"backspace");
			consoleContainerText.getActionMap().put("backspace", backspace);
			// BackSpace

			// Enter
			Action enter = new AbstractAction()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					if ( !EDITABLE )
					{
						return;
					}
					try
					{
						input = consoleDocumentStyle.getText(inputStart, consoleDocumentStyle.getLength() - inputStart);
						lastInputStart = inputStart;
						lastInputEnd = consoleDocumentStyle.getLength();
					}
					catch ( BadLocationException ex )
					{
						System.err.println(ex.getMessage());
						System.exit(1);
					}
					EDITABLE = false;
					promptVal = null;
					SELECTED = false;
					maxInput = -1;

					consoleContainerText.setEditable(false);
					consoleContainerCaret.setVisible(false);
					writeMsg(newLine);
					latch.countDown();
				}
			};
			consoleContainerText.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), "enter");
			consoleContainerText.getActionMap().put("enter", enter);
			// Enter

			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension frameSize = new Dimension((int) (screenSize.width / 2), (int) (screenSize.height / 2));
			int x = (int) (frameSize.width / 2);
			int y = (int) (frameSize.height / 2);
			consoleFrame.setBounds(x, y, frameSize.width, frameSize.height);
			consoleFrame.add(new JScrollPane(consoleContainerText));
			consoleFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			consoleFrame.setResizable(false);
			// consoleFrame.setLocationRelativeTo(null);
			consoleFrame.setVisible(true);
		}

		public void setDefaultCloseOperation(int option)
		{
			consoleFrame.setDefaultCloseOperation(option);
		}

		public void gotoEnd()
		{
			goTo(consoleDocumentStyle.getLength());
		}

		private void goTo(final int index)
		{
			SwingUtilities.invokeLater(new Runnable()
			{

				@Override
				public void run()
				{
					consoleContainerCaret.setDot(index);
				}

			});
		}

		public void setCaretColor(final Color color)
		{
			SwingUtilities.invokeLater(new Runnable()
			{

				@Override
				public void run()
				{
					consoleContainerText.setCaretColor(color);
				}

			});
		}

		private void setFont(final String font, final int style, final int size)
		{
			SwingUtilities.invokeLater(new Runnable()
			{

				@Override
				public void run()
				{
					consoleContainerText.setFont(new Font(font, style, size));
				}

			});
		}

		public void setTitle(final String title)
		{
			SwingUtilities.invokeLater(new Runnable()
			{

				@Override
				public void run()
				{
					consoleFrame.setTitle(title);
				}

			});
		}

		public void clear()
		{
			consoleContainerText.setText("");
		}

		public void print()
		{
			writeMsg(" ");
		}

		public void print(String msg)
		{
			writeMsg(msg);
		}

		public void print(char c)
		{
			writeMsg(String.valueOf(c));
		}

		public void print(int i)
		{
			writeMsg(String.valueOf(i));
		}

		public void print(int i[])
		{
			int a;
			for ( a = 0; a < i.length; a++ )
			{
				writeMsg(String.valueOf(i[a]) + " ");
			}
		}

		public void print(double d)
		{
			writeMsg(String.valueOf(d));
		}

		public void print(Object obj)
		{
			writeMsg(String.valueOf(obj));
		}

		public void print(float t)
		{
			writeMsg(String.valueOf(t));
		}

		public void print(long l)
		{
			writeMsg(String.valueOf(l));
		}

		public void println()
		{
			writeMsg(newLine);
		}

		public void println(String msg)
		{
			writeMsg(msg + newLine);
		}

		public void println(char c)
		{
			writeMsg(String.valueOf(c) + newLine);
		}

		public void println(int i)
		{
			writeMsg(String.valueOf(i) + newLine);
		}

		public void println(int i[])
		{
			int a;
			for ( a = 0; a < i.length; a++ )
			{
				writeMsg(String.valueOf(i[a]) + " ");
			}
			writeMsg(newLine);
		}

		public void println(double d)
		{
			writeMsg(String.valueOf(d) + newLine);
		}

		public void println(Object obj)
		{
			writeMsg(String.valueOf(obj) + newLine);
		}

		public void println(float t)
		{
			writeMsg(String.valueOf(t) + newLine);
		}

		public void println(long l)
		{
			writeMsg(String.valueOf(l) + newLine);
		}

		public String nextPassword()
		{
			return passwordPrompt();
		}

		public String nextPassword(int l)
		{
			maxInput = l;
			return passwordPrompt();
		}

		public String nextLine()
		{
			return prompt();
		}

		public String nextLine(int l)
		{
			maxInput = l;
			return prompt();
		}

		public double nextDouble()
		{
			return Double.valueOf(prompt());
		}

		public double nextDouble(int l)
		{
			maxInput = l;
			return Double.valueOf(prompt());
		}

		public float nextFloat()
		{
			return Float.valueOf(prompt());
		}

		public float nextFloat(int l)
		{
			maxInput = l;
			return Float.valueOf(prompt());
		}

		public long nextLong()
		{
			return Long.valueOf(prompt());
		}

		public long nextLong(int l)
		{
			maxInput = l;
			return Long.valueOf(prompt());
		}

		public int nextInt()
		{
			return Integer.valueOf(prompt());
		}

		public int nextInt(int l)
		{
			maxInput = l;
			return Integer.valueOf(prompt());
		}

		public void setPromptVal(String val, boolean s)
		{
			promptVal = val;
			SELECTED = s;
		}

		private String passwordPrompt()
		{
			setColor(BACKGROUNDCOLOR);
			String inp = prompt();
			return inp;
		}

		private String prompt()
		{
			writeMsg("\r");
			inputStart = consoleDocumentStyle.getLength();
			EDITABLE = true;
			SwingUtilities.invokeLater(new Runnable()
			{

				@Override
				public void run()
				{
					consoleContainerText.setEditable(EDITABLE);
					consoleContainerCaret.setVisible(true);
					consoleContainerCaret.setDot(inputStart);
				}

			});
			if ( promptVal != null )
			{
				writeMsg(promptVal);
				if ( SELECTED )
				{
					select(inputStart, consoleDocumentStyle.getLength());
				}
			}
			try
			{
				latch = new CountDownLatch(1);
				latch.await();
			}
			catch ( java.lang.InterruptedException ex )
			{
				System.err.print("The latch failed:" + "\n" + ex.getMessage());
			}
			return input;
		}

		public void setColor(Color c)
		{
			FONTCOLOR = c;
		}

		public void setBackgroundColor(Color c)
		{
			BACKGROUNDCOLOR = c;
		}

		public void setBold(boolean b)
		{
			BOLD = b;
		}

		public void setPrefix(String p)
		{
			PREFIX = p;
		}

		public void setSuffix(String s)
		{
			SUFFIX = s;
		}

		private void writeMsg(String texto)
		{

			StyleConstants.setForeground(consoleStyle, FONTCOLOR);
			StyleConstants.setBackground(consoleStyle, BACKGROUNDCOLOR);
			StyleConstants.setBold(consoleStyle, BOLD);

			try
			{
				consoleDocumentStyle.insertString(consoleDocumentStyle.getLength(), texto, consoleStyle);
				consoleContainerText.setCaretPosition(consoleDocumentStyle.getLength());
			}
			catch ( BadLocationException ex )
			{
				println("Error:");
				println(ex.getMessage());
			}

			FONTCOLOR = DEFAULT_COLOR_T;
			BACKGROUNDCOLOR = DEFAULT_COLOR_B;
			BOLD = false;
		}

		private void replaceRange(String str, int start, int end)
		{
			try
			{
				if ( consoleDocumentStyle instanceof AbstractDocument )
				{
					((AbstractDocument) consoleDocumentStyle).replace(start, end - start, str, null);
				}
				else
				{
					consoleDocumentStyle.remove(start, end - start);
					consoleDocumentStyle.insertString(start, str, null);
				}
			}
			catch ( BadLocationException e )
			{
				throw new IllegalArgumentException(e.getMessage());
			}
		}

		private void select(final int start, final int end)
		{
			SwingUtilities.invokeLater(new Runnable()
			{

				@Override
				public void run()
				{
					consoleContainerText.select(start, end);
				}

			});
		}

		private void redirectSystemStreams()
		{
			OutputStream out = new OutputStream()
			{
				@Override
				public void write(final int b) throws IOException
				{
					writeMsg(String.valueOf((char) b));
				}

				@Override
				public void write(byte [] b, int off, int len) throws IOException
				{
					writeMsg(new String(b, off, len));
				}

				@Override
				public void write(byte [] b) throws IOException
				{
					write(b, 0, b.length);
				}
			};
			System.setOut(new PrintStream(out, true));
			System.setErr(new PrintStream(out, true));
		}

		private class InputPolicy implements KeyListener, ChangeListener
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				if ( !EDITABLE )
				{
					return;
				}
				int maxRegion = inputStart + maxInput;
				if ( consoleContainerCaret.getDot() < inputStart || consoleContainerCaret.getMark() < inputStart
						|| (maxInput > -1 && consoleDocumentStyle.getLength() >= maxRegion) )
				{
					consoleContainerText.setEditable(false);
				}
				else if ( !consoleContainerText.isEditable() )
				{
					consoleContainerText.setEditable(true);
				}
			}

			@Override
			public void keyTyped(KeyEvent e)
			{
				if ( !EDITABLE )
				{
					return;
				}
				if ( consoleContainerCaret.getDot() < inputStart && EDITABLE )
				{
					consoleContainerCaret.setDot(consoleContainerText.getText().length());
				}
			}

			@Override
			public void keyPressed(KeyEvent e)
			{
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
			}

		}
	}

}
