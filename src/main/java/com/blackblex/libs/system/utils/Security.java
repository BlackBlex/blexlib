/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: Security.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class Security
{

	private SecretKey key;
	private Cipher cipher;
	private String algoritmo = "AES";
	private int keysize = 16;

	public void addKey(String value)
	{
		byte [] valuebytes = value.getBytes();
		key = new SecretKeySpec(Arrays.copyOf(valuebytes, keysize), algoritmo);
	}

	public String encriptar(String texto)
	{
		String value = "";
		try
		{
			cipher = Cipher.getInstance(algoritmo);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte [] textobytes = texto.getBytes();
			byte [] cipherbytes = cipher.doFinal(textobytes);
			value = Base64.getEncoder().encodeToString(cipherbytes);
		}
		catch ( NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
				| BadPaddingException ex )
		{
			System.err.println(ex.getMessage());
		}
		return value;
	}

	public String desencriptar(String texto)
	{
		String str = "";
		try
		{
			byte [] value = Base64.getDecoder().decode(texto);
			cipher = Cipher.getInstance(algoritmo);
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte [] cipherbytes = cipher.doFinal(value);
			str = new String(cipherbytes);
		}
		catch ( InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
				| NoSuchPaddingException ex )
		{
			System.err.println(ex.getMessage());
		}
		return str;
	}
}
