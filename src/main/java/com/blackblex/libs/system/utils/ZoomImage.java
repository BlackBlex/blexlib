/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: ZoomImage.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class ZoomImage extends JPanel
{

	private static final long serialVersionUID = -5431106194219985327L;
	private Image FOTO_ORIGINAL;
	private Image FOTO_tmp;
	private BufferedImage Imagen_en_memoria;
	private Graphics2D g2D;
	private boolean con_foto = false;
	private Point inicialPoint;
	private int PosX = 0, PosY = 0;
	private int TamH = 0, TamW = 0;

	private int valEscalaX = 0;
	private int valEscalaY = 0;

	public ZoomImage(BufferedImage f)
	{
		this.FOTO_ORIGINAL = f;
		this.FOTO_tmp = f;
		this.TamH = f.getHeight();
		this.TamW = f.getWidth();
		this.setSize(f.getWidth(), f.getHeight());
		this.setVisible(true);
		this.con_foto = true;		
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));

		this.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent evt)
			{
				inicialPoint = getScreenLocation(evt);
				setCursor(new Cursor(Cursor.MOVE_CURSOR));
			}

			@Override
			public void mouseReleased(MouseEvent evt)
			{
				inicialPoint = getScreenLocation(evt);
				setCursor(new Cursor(Cursor.HAND_CURSOR));
			}
		});

		this.addMouseMotionListener(new MouseMotionAdapter()
		{
			@Override
			public void mouseDragged(MouseEvent evt)
			{
				int thisX = getScreenLocation(evt).x;
				int thisY = getScreenLocation(evt).y;

				int sizeX = (int) (getSize().getWidth());
				int sizeY = (int) (getSize().getHeight());

				int xMoved = (thisX + (int)(getLocation().getX())) - (thisX + inicialPoint.x);
				int yMoved = (thisY + (int)(getLocation().getY())) - (thisY + inicialPoint.y);

				if ( ((thisX + xMoved) <= 0) && ((thisX + xMoved + TamW) >= sizeX) )
				{
					PosX = thisX + xMoved;
				}

				if ( ((thisY + yMoved) <= 0) && ((thisY + yMoved + TamH) >= sizeY) )
				{
					PosY = thisY + yMoved;
				}

				repaint();
			}
		});
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		if ( this.con_foto )
		{
			Imagen_en_memoria = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
			g2D = Imagen_en_memoria.createGraphics();
			g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2D.drawImage(FOTO_tmp, PosX, PosY, FOTO_tmp.getWidth(this), FOTO_tmp.getHeight(this), this);
			g2.drawImage(Imagen_en_memoria, 0, 0, this);
		}
	}

	public void Aumentar(int Valor_Zoom)
	{
		valEscalaX = (int) (FOTO_tmp.getWidth(this) * escala(Valor_Zoom));
		valEscalaY = (int) (FOTO_tmp.getHeight(this) * escala(Valor_Zoom));
		this.FOTO_tmp = FOTO_tmp.getScaledInstance((int) (FOTO_tmp.getWidth(this) + valEscalaX),
				(int) (FOTO_tmp.getHeight(this) + valEscalaY), Image.SCALE_AREA_AVERAGING);
		resize();
	}

	public void Disminuir(int Valor_Zoom)
	{
		valEscalaX = (int) (FOTO_tmp.getWidth(this) * escala(Valor_Zoom));
		valEscalaY = (int) (FOTO_tmp.getHeight(this) * escala(Valor_Zoom));
		this.FOTO_tmp = FOTO_tmp.getScaledInstance((int) (FOTO_tmp.getWidth(this) - valEscalaX),
				(int) (FOTO_tmp.getHeight(this) - valEscalaY), Image.SCALE_AREA_AVERAGING);
		resize();
	}

	private float escala(int v)
	{
		return v / 100f;
	}

	public void Restaurar()
	{
		this.FOTO_tmp = this.FOTO_ORIGINAL;
		resize();
	}

	private void resize()
	{
		this.setSize(FOTO_tmp.getWidth(this), FOTO_tmp.getHeight(this));
	}

	private Point getScreenLocation(MouseEvent evt)
	{
		Point cursor = evt.getPoint();
		Point target_location = this.getLocationOnScreen();
		return new Point((int) (target_location.getX() + cursor.getX()),
				(int) (target_location.getY() + cursor.getY()));
	}
}
