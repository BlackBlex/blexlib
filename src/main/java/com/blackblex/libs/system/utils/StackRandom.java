/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils
 *
 * ==============Information==============
 *      Filename: StackRandom.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils;

import java.util.ArrayList;
import java.util.Random;

public class StackRandom<T>
{

	private ArrayList <T> elements;
	private Random rn;

	public StackRandom()
	{
		this.elements = new ArrayList <>();
		this.rn = new Random();
	}

	public void add(T element)
	{
		this.elements.add(element);
	}

	public T select(int index)
	{
		return this.elements.get(index);
	}

	public T selectNext()
	{
		return this.elements.get(rn.nextInt(this.elements.size()));
	}

	public int lenght()
	{
		return (this.elements.size());
	}

	public T pop()
	{
		T last = this.elements.get((this.elements.size() - 1));
		this.elements.remove((this.elements.size() - 1));
		return last;
	}

}
