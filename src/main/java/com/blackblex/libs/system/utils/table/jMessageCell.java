/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.utils.table
 *
 * ==============Information==============
 *      Filename: jMessageCell.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.utils.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.blackblex.libs.net.objects.jMessage;

public class jMessageCell extends AbstractCellEditor implements TableCellEditor, TableCellRenderer
{

	JPanel panel;

	GridBagConstraints gb = new GridBagConstraints();
	JTextPane jtext = new JTextPane();
	JLabel jfrom = new JLabel("");
	jMessage item;

	public jMessageCell()
	{
		panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(20, 15, 15, 15));

		panel.setLayout(new GridBagLayout());

		gb.fill = GridBagConstraints.BOTH;
		gb.gridx = 0;
		gb.gridy = 0;
		gb.weightx = 1.0;
		panel.add(jfrom, gb);

		gb.gridy = 1;
		panel.add(jtext, gb);
	}

	private void updateData(jMessage item, boolean isSelected, JTable table)
	{
		this.item = item;

		if ( this.item.getFrom() != null && this.item.getStatus() != 2 && this.item.getStatus() != 0 )
			jfrom.setText(this.item.getFrom().getUsername() + " dijo:");
		else
			jfrom.setText("");

		jtext.setDocument(this.item.getJMessage());

		StyledDocument docd = jtext.getStyledDocument();
		SimpleAttributeSet align = new SimpleAttributeSet();

		switch ( this.item.getStatus() )
		{
			case 0:
				StyleConstants.setAlignment(align, StyleConstants.ALIGN_RIGHT);
				jtext.setBackground(Color.decode("#DCF8C6"));
				panel.setBackground(Color.decode("#DCF8C6"));
				break;
			case 1:
				StyleConstants.setAlignment(align, StyleConstants.ALIGN_LEFT);
				jtext.setBackground(Color.WHITE);
				panel.setBackground(Color.WHITE);
				break;
			case 2:
				StyleConstants.setAlignment(align, StyleConstants.ALIGN_CENTER);
				jtext.setBackground(Color.decode("#FEF3C4"));
				panel.setBackground(Color.decode("#FEF3C4"));
				break;
		}

		docd.setParagraphAttributes(0, docd.getLength(), align, false);

	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
	{
		jMessage item = (jMessage) value;
		updateData(item, true, table);
		return this.panel;
	}

	@Override
	public Object getCellEditorValue()
	{
		return null;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column)
	{
		jMessage item = (jMessage) value;

		try
		{
			int lines = item.getJMessage().getLength() / 100;
			if ( lines == 0 )
				lines = 1;

			int height = 0;
			if ( item.getFrom() != null && item.getStatus() != 2 && item.getStatus() != 0 )
				height = lines * 50;
			else
				height = lines * 35;
			table.setRowHeight(row, height);
			// jtext.setPreferredSize(new Dimension(jtext.getWidth(), height));
			// jtext.setMinimumSize(new Dimension(jtext.getWidth(), height));
		}
		catch ( NullPointerException ex )
		{
		}

		updateData(item, isSelected, table);
		return this.panel;
	}
}
