/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.reflection
 *
 * ==============Information==============
 *      Filename: Information.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.reflection;

import java.lang.reflect.Field;
import java.util.ArrayList;

import com.blackblex.libs.main.Init;

public class Information<T>
{

	private T objectInformation;
	private Class <? extends T> classInformation;
	private ArrayList <FieldInformation> fieldInformations = null;

	protected String packageName = null, canonicalPackageName = null;

	public Information(T obj)
	{
		this.objectInformation = obj;
		this.classInformation = (Class <? extends T>) this.objectInformation.getClass();
	}
	
	public String getName()
	{
		return this.classInformation.getSimpleName();
	}

	public String getPackage()
	{
		if ( this.packageName == null )
		{
			String packageName = this.classInformation.getPackage().getName();
			String packageNameA[];
			if ( packageName.contains(".") )
			{
				packageNameA = packageName.split("\\.");
				packageName = packageNameA[packageNameA.length - 1];
			}
			this.packageName = packageName;
		}

		return this.packageName;

	}

	public String getCanonicalPackage()
	{
		if ( this.canonicalPackageName == null )
			this.canonicalPackageName = this.classInformation.getPackage().getName();

		return this.canonicalPackageName;
	}

	public FieldInformation [] getFields()
	{
		if ( this.fieldInformations == null )
		{
			this.fieldInformations = new ArrayList <FieldInformation>();

			for ( Field field : this.classInformation.getDeclaredFields() )
				this.fieldInformations.add(new FieldInformation(field, this.objectInformation));

		}

		return this.fieldInformations.toArray(new FieldInformation [0]);
	}

	public FieldInformation getField(String fieldName)
	{
		Field field = null;
		try
		{
			field = this.classInformation.getField(fieldName);
		}
		catch ( NoSuchFieldException | SecurityException e )
		{
			try
			{
				field = this.classInformation.getDeclaredField(fieldName);
			}
			catch ( NoSuchFieldException | SecurityException e1 )
			{
			}
		}
		if ( field != null )
			return new FieldInformation(field, this.objectInformation);
		else
			return null;
	}
	
}
