/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.sqlite
 *
 * ==============Information==============
 *      Filename: OperationSQL.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.sqlite;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.utils.MultiString;
import java.sql.ResultSetMetaData;

public class OperationSQL
{

	public Map <String, String> rows = new LinkedHashMap <>();
	public Map <MultiString, MultiString> wheres = new LinkedHashMap <>();
	public Map <String, String> order = new LinkedHashMap <>();

	private ConnectionBD connect;
	private String database = "";

	public OperationSQL(String database)
	{
		this.database = database;
	}

	public Date formatDate(String data)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date parsed = null;
		try
		{
			parsed = format.parse(data);
		}
		catch ( ParseException e )
		{
			Init.MSG.printError(e);
		}
		return new Date(parsed.getTime());
	}

	public Time formatTime(String data)
	{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		java.util.Date parsed = null;
		try
		{
			parsed = format.parse(data);
		}
		catch ( ParseException e )
		{
			Init.MSG.printError(e);
		}
		return new Time(parsed.getTime());
	}

	public Date getDate()
	{
		return new java.sql.Date(Calendar.getInstance().getTime().getTime());
	}

	public Time getTime()
	{
		return new java.sql.Time(Calendar.getInstance().getTime().getTime());
	}

	public boolean exec(String sql)
	{
		connect = new ConnectionBD(this.database);
		Connection c = connect.getConnection();
		Statement s;
		try
		{
			s = c.createStatement();
			s.executeUpdate(sql);
			s.close();
			c.close();
			return true;
		}
		catch ( SQLException e )
		{
			Init.MSG.printError(e);
			return false;
		}
	}

	public void insert(String from)
	{
		if ( !rows.isEmpty() )
		{
			connect = new ConnectionBD(this.database);
			Connection c = connect.getConnection();
			Statement s;
			String tables = "";
			String values = "";
			int counter = 1;
			for ( Entry <String, String> entries : rows.entrySet() )
			{
				String column = entries.getKey();
				String row = entries.getValue();
				if ( rows.size() == counter )
				{
					tables += column;
					if ( Init.isNumeric(row, "L") )
					{
						values += row;
					}
					else
					{
						values += "'" + row + "'";
					}
				}
				else
				{
					tables += column + ",";
					if ( Init.isNumeric(row, "L") )
					{
						values += row + ",";
					}
					else
					{
						values += "'" + row + "',";
					}
				}
				counter++;
			}

			String sql = "INSERT INTO " + from + " (" + tables + ") VALUES (" + values + ");";

			try
			{
				s = c.createStatement();
				s.executeUpdate(sql);
				s.close();
				c.close();
				Init.MSG.printMsg("Insertado con exito!");
			}
			catch ( SQLException e )
			{
				Init.MSG.printError(e);
			}
		}

		clearAll();
	}

	public boolean update(String from)
	{

		connect = new ConnectionBD(this.database);
		Connection c = connect.getConnection();
		Statement s;
		String tables = "", whers = "";
		int counter = 1;
		if ( !rows.isEmpty() )
		{
			for ( Entry <String, String> entries : rows.entrySet() )
			{
				String column = entries.getKey();
				String row = entries.getValue();
				if ( rows.size() == counter )
				{
					if ( Init.isNumeric(row, "L") )
					{
						tables += column + " = " + row;
					}
					else
					{
						tables += column + " = '" + row + "'";
					}
				}
				else if ( Init.isNumeric(row, "L") )
				{
					tables += column + " = " + row + ", ";
				}
				else
				{
					tables += column + " = '" + row + "', ";
				}
				counter++;
			}
		}

		if ( !wheres.isEmpty() )
		{
			for ( Entry <MultiString, MultiString> entries : wheres.entrySet() )
			{
				MultiString where = entries.getKey();
				MultiString operation = entries.getValue();
				String row;
				if ( Init.isNumeric(where.getTwo(), "L") )
				{
					row = where.getTwo();
				}
				else
				{
					row = "'" + where.getTwo() + "'";
				}
				whers += where.getOne() + " " + operation.getOne() + " " + row + " " + operation.getTwo() + " ";
			}
		}

		String sql;
		if ( !wheres.isEmpty() )
		{
			sql = "UPDATE " + from + " SET " + tables + " WHERE " + whers;
		}
		else
		{
			sql = "UPDATE " + from + " SET " + tables;
		}

		try
		{
			s = c.createStatement();
			if ( s.executeUpdate(sql) == 1 )
			{
				Init.MSG.printMsg("Actualizado con exito!");
				s.close();
				c.close();

				clearAll();
				return true;
			}
			else
			{
				Init.MSG.printMsg("Actualizado sin exito!");
				s.close();
				c.close();

				//clearAll();
				return false;
			}
		}
		catch ( SQLException e )
		{
			Init.MSG.printError(e);
		}
		return false;
	}

	public boolean delete(String from)
	{

		connect = new ConnectionBD(this.database);
		Connection c = connect.getConnection();
		Statement s;
		String whers = "";
		if ( !wheres.isEmpty() )
		{
			for ( Entry <MultiString, MultiString> entries : wheres.entrySet() )
			{
				MultiString where = entries.getKey();
				MultiString operation = entries.getValue();
				String row;
				if ( Init.isNumeric(where.getTwo(), "L") )
				{
					row = where.getTwo();
				}
				else
				{
					row = "'" + where.getTwo() + "'";
				}

				if ( where.getTwo().isEmpty() )
				{
					return false;
				}
				whers += where.getOne() + " " + operation.getOne() + " " + row + " " + operation.getTwo() + " ";
			}
		}

		String sql = "";
		if ( !wheres.isEmpty() )
		{
			sql = "DELETE FROM " + from + " WHERE " + whers;
		}

		try
		{
			s = c.createStatement();
			if ( s.executeUpdate(sql) == 1 )
			{
				Init.MSG.printMsg("Eliminado con exito!");
				s.close();
				c.close();

				clearAll();
				return true;
			}
			else
			{
				Init.MSG.printMsg("Eliminado sin exito!");
				s.close();
				c.close();

				//clearAll();
				return false;
			}

		}
		catch ( SQLException e )
		{
			Init.MSG.printError(e);
		}
		return false;
	}

	public MultiString [] select(String from)
	{
		MultiString [] result = null;
		String tables = "", whers = "", orders = "";

		connect = new ConnectionBD(this.database);
		Connection c = connect.getConnection();
		Statement s;
		ResultSet r;
		int rowsCounter = 0;
		if ( !rows.isEmpty() )
		{
			int counter = 1;
			for ( Entry <String, String> entries : rows.entrySet() )
			{
				String column = entries.getKey();
				String row = entries.getValue();
				if ( rows.size() == counter )
				{
					if ( row != "" )
					{
						tables += column + ", " + row;
						rowsCounter += 2;
					}
					else
					{
						tables += column;
						rowsCounter += 1;
					}
				}
				else
				{
					tables += column + ", ";
					tables += row + ", ";
					rowsCounter += 2;
				}
				counter++;
			}
		}

		if ( !wheres.isEmpty() )
		{
			for ( Entry <MultiString, MultiString> entries : wheres.entrySet() )
			{
				MultiString where = entries.getKey();
				MultiString operation = entries.getValue();
				String row;
				if ( Init.isNumeric(where.getTwo(), "L") )
				{
					row = where.getTwo();
				}
				else
				{
					row = "'" + where.getTwo() + "'";
				}
				whers += where.getOne() + " " + operation.getOne() + " " + row + " " + operation.getTwo() + " ";
			}
		}

		if ( !order.isEmpty() )
		{
			for ( Entry <String, String> entries : order.entrySet() )
			{
				orders = "ORDER BY " + entries.getKey() + " " + entries.getValue();
			}
		}

		String sql, sqlC;
		if ( !wheres.isEmpty() )
		{
			sql = "SELECT " + tables + " FROM " + from + " WHERE " + whers;
			sqlC = "SELECT COUNT(*) AS rowcount FROM " + from + " WHERE " + whers;
		}
		else
		{
			sql = "SELECT " + tables + " FROM " + from;
			sqlC = "SELECT COUNT(*) AS rowcount FROM " + from;
		}

		if ( !order.isEmpty() )
		{
			sql = sql + " " + orders;
		}

		boolean correct = true;

		int i = 0, j;
		try
		{
			s = c.createStatement();
			r = s.executeQuery(sqlC);
			int resultRows = r.getInt("rowcount");
			r = s.executeQuery(sql);
			result = new MultiString [resultRows];

			while ( r.next() )
			{
				result[i] = new MultiString();
				for ( j = 1; j <= rowsCounter; j++ )
				{
					result[i].set(j, r.getString(j));
				}
				i++;
			}

			r.close();
			s.close();
			c.close();

		}
		catch ( SQLException e )
		{
			e.printStackTrace();
			// Init.MSG.printError(e);
		}
		if ( i == 0 )
		{
			correct = false;
		}

		clearAll();

		if ( correct )
		{
			return result;
		}
		else
		{
			return null;
		}
	}

	public MultiString [] selectAll(String from)
	{
		MultiString [] result = null;
		String whers = "", orders = "";

		connect = new ConnectionBD(this.database);
		Connection c = connect.getConnection();
		Statement s;
		ResultSet r;

		if ( !wheres.isEmpty() )
		{
			for ( Entry <MultiString, MultiString> entries : wheres.entrySet() )
			{
				MultiString where = entries.getKey();
				MultiString operation = entries.getValue();
				String row = "";
				if ( Init.isNumeric(where.getTwo(), "L") )
				{
					row = where.getTwo();
				}
				else if ( !where.getTwo().isEmpty() )
				{
					row = "'" + where.getTwo() + "'";
				}
				whers += where.getOne() + " " + operation.getOne() + " " + row + " " + operation.getTwo() + " ";
			}
		}

		if ( !order.isEmpty() )
		{
			for ( Entry <String, String> entries : order.entrySet() )
			{
				orders = "ORDER BY " + entries.getKey() + " " + entries.getValue();
			}
		}

		String sql, sqlC;
		if ( !wheres.isEmpty() )
		{
			sql = "SELECT * FROM " + from + " WHERE " + whers;
			sqlC = "SELECT COUNT(*) AS rowcount FROM " + from + " WHERE " + whers;
		}
		else
		{
			sql = "SELECT * FROM " + from;
			sqlC = "SELECT COUNT(*) AS rowcount FROM " + from;
		}

		if ( !order.isEmpty() )
		{
			sql = sql + " " + orders;
		}

		boolean correct = true;

		int i = 0, j;
		try
		{
			s = c.createStatement();
			r = s.executeQuery(sqlC);
			int resultRows = r.getInt("rowcount");
			r = s.executeQuery(sql);
			ResultSetMetaData rsmd = r.getMetaData();
			result = new MultiString [resultRows];
			int rowsCounter = rsmd.getColumnCount();

			while ( r.next() )
			{
				result[i] = new MultiString();
				for ( j = 1; j <= rowsCounter; j++ )
				{
					result[i].set(j, r.getString(j));
				}
				i++;
			}

			r.close();
			s.close();
			c.close();

		}
		catch ( SQLException e )
		{
			e.printStackTrace();
			// Init.MSG.printError(e);
		}
		if ( i == 0 )
		{
			correct = false;
		}

		clearAll();

		if ( correct )
		{
			return result;
		}
		else
		{
			return null;
		}
	}

	public void clearAll()
	{
		wheres.clear();
		order.clear();
		rows.clear();
	}

}
