/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.system.sqlite
 *
 * ==============Information==============
 *      Filename: ConnectionBD.java
 * ---------------------------------------
*/

package com.blackblex.libs.system.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.blackblex.libs.main.Init;

class ConnectionBD
{

	private String
		server = "jdbc:sqlite:";

	private Connection connection = null;

	public ConnectionBD(String url)
	{
		this.server += url;
	}

	public Connection getConnection()
	{
		if ( this.connection == null )
		{
			try
			{
				this.connection = DriverManager.getConnection(this.server);
				return this.connection;
			}
			catch ( SQLException e )
			{
				Init.MSG.printError(e);
				this.connection = null;
				return this.connection;
			}
		}
		else
		{
			return this.connection;
		}
	}

	public void close()
	{
		try
		{
			this.connection.close();
		}
		catch ( SQLException e )
		{
			Init.MSG.printError(e);
		}
	}
}
