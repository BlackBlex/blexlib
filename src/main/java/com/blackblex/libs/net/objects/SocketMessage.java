/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.net.objects
 *
 * ==============Information==============
 *      Filename: SocketMessage.java
 * ---------------------------------------
*/

package com.blackblex.libs.net.objects;

import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.utils.Observable;
import java.io.Serializable;
import javax.swing.text.Document;

public class SocketMessage extends Observable implements Serializable
{

	private static final long serialVersionUID = 121395121395121395L;
	private String message, action;
	private Document jmessage;
	private int from = -1, to = -1;

	@Override
	public void action()
	{

	}

	@Override
	public String actionString()
	{
		return getAction() + "-/-" + getFrom() + "-/-" + getTo();
	}

	public String getMessage()
	{
		return this.message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public Document getjMessage()
	{
		return this.jmessage;
	}

	public void setjMessage(Document message)
	{
		this.jmessage = (Document) Init.cloneObject(message);
	}

	public int getTo()
	{
		return this.to;
	}

	public void setTo(int to)
	{
		this.to = to;
	}

	public int getFrom()
	{
		return this.from;
	}

	public void setFrom(int from)
	{
		this.from = from;
	}

	public String getAction()
	{
		return this.action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	private String fileName = "";
	private String filePath = "";
	private String fileContent = "";
	private long fileSize = 0L;
	private String fileType = "";

	public String getFileName()
	{
		return this.fileName;
	}

	public void setFileName(String name)
	{
		this.fileName = name;
	}

	public String getFileContent()
	{
		return this.fileContent;
	}

	public void setFileContent(String content)
	{
		this.fileContent = content;
	}

	public long getFileSize()
	{
		return this.fileSize;
	}

	public void setFileSize(long size)
	{
		this.fileSize = size;
	}

	public String getFileType()
	{
		return this.fileType;
	}

	public void setFileType(String type)
	{
		this.fileType = type;
	}

	public String getFilePath()
	{
		return this.filePath;
	}

	public void setFilePath(String path)
	{
		this.filePath = path;
	}
}
