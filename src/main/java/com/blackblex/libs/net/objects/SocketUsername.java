/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.net.objects
 *
 * ==============Information==============
 *      Filename: SocketUsername.java
 * ---------------------------------------
*/

package com.blackblex.libs.net.objects;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class SocketUsername
{
	private int idSession;

	private Socket socketServer, socketClient;

	private String username;

	private boolean newsocket = true;

	private ObjectOutputStream objectOutput;

	public Socket getSocketClient()
	{
		return this.socketClient;
	}

	public void setSocketClient(Socket socket)
	{
		this.socketClient = socket;
	}

	public Socket getSocketServer()
	{
		return this.socketServer;
	}

	public void setSocketServer(Socket socket)
	{
		this.socketServer = socket;
	}

	public String getUsername()
	{
		return this.username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public int getIdSession()
	{
		return this.idSession;
	}

	public void setIdSession(int idSession)
	{
		this.idSession = idSession;
	}

	public boolean isNewsocket()
	{
		return this.newsocket;
	}

	public void setNewSocket(boolean newsocket)
	{
		this.newsocket = newsocket;
	}

	public void sendClient(SocketMessage dataOutput)
	{
		try
		{
			this.objectOutput = new ObjectOutputStream(getSocketClient().getOutputStream());
			this.objectOutput.writeObject(dataOutput);
			dataOutput.notifyObs();
		}
		catch ( IOException e )
		{
			System.out.println("[SocketUsername sendClient]: " + e.getMessage());
		}
	}

	public void sendServer(SocketMessage dataOutput)
	{
		try
		{
			this.objectOutput = new ObjectOutputStream(getSocketServer().getOutputStream());
			this.objectOutput.writeObject(dataOutput);
			dataOutput.notifyObs();
		}
		catch ( IOException e )
		{
			System.out.println("[SocketUsername sendServer]: " + e.getMessage());
		}
	}

	@Override
	public String toString()
	{
		return "[" + this.idSession + "] " + this.username;
	}

}
