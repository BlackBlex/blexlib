/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.net.objects
 *
 * ==============Information==============
 *      Filename: jMessage.java
 * ---------------------------------------
*/

package com.blackblex.libs.net.objects;

import javax.swing.JTextPane;
import javax.swing.text.Document;

public class jMessage {
    
    private int status;
    private SocketUsername from = null;
    private Document document;
    private String message;
    
    public jMessage(int status)
    {
        this.status = status;
    }
    
    public int getStatus()
    {
        return this.status;
    }
    
    public void setMessage(String msg)
    {
        JTextPane jte = new JTextPane();
        jte.setText(msg);
        this.document = jte.getDocument();
    }
    
    public String getMessage()
    {
        return this.message;
    }
    
    public Document getJMessage()
    {
        return this.document;
    }
    
    public void setJMessage(Document doc, SocketUsername from)
    {
        this.from = from;
        this.document = doc;
    }
    
    public void setJMessage(Document doc)
    {
        this.document = doc;
    }

    public SocketUsername getFrom() 
    {
        return from;
    }
    
}
