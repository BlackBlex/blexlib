/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.main
 *
 * ==============Information==============
 *      Filename: Init.java
 * ---------------------------------------
*/

package com.blackblex.libs.main;

import com.blackblex.libs.system.utils.Messages;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.swing.JTextArea;

public class Init
{

	public static Messages MSG = new Messages();

	private Init()
	{

	}

	public static String getDate()
	{
		SimpleDateFormat _sdf = new SimpleDateFormat("dd-MM-yyyy");
		return _sdf.format(new Date());
	}

	public static int getSize(String number)
	{
		if ( number.length() == 0 )
		{
			return 0;
		}

		try
		{
			Long i = Long.parseLong(number);
			return i.toString().length();
		}
		catch ( NumberFormatException e )
		{
			MSG.printError(e);
			return 0;
		}
	}

	public static boolean isNumeric(String number, String type)
	{
		try
		{
			switch ( type )
			{
				case "L":
					Long.parseLong(number);
					break;
				case "I":
					Integer.parseInt(number);
					break;
				case "D":
					Double.parseDouble(number);
					break;
			}
		}
		catch ( NumberFormatException | NullPointerException e )
		{
			return false;
		}

		return true;
	}

	public static String getRandomCode(int limit)
	{
		String codeResult = "";
		long miliseconds = new java.util.GregorianCalendar().getTimeInMillis();
		Random random = new Random(miliseconds);
		int i = 0;
		while ( i < limit )
		{
			char character = (char) random.nextInt(255);
			if ( (character >= '0' && character <= '9') || (character >= 'A' && character <= 'D') )
			{
				codeResult += character;
				i++;
			}
		}
		return codeResult;
	}

	public static String getRandomCode()
	{
		return getRandomCode(8);
	}

	public static String getRandomID(int limit)
	{
		String codeResult = "";
		long miliseconds = new java.util.GregorianCalendar().getTimeInMillis();
		Random random = new Random(miliseconds);
		int i = 0;
		while ( i < limit )
		{
			char character = (char) random.nextInt(255);
			if ( (character >= '0' && character <= '9') )
			{
				codeResult += character;
				i++;
			}
		}
		return codeResult;
	}

	public static String getRandomID()
	{
		return getRandomID(9);
	}

	public static void redirectSystemStream(final JTextArea jtext)
	{
		OutputStream out = new OutputStream()
		{
			@Override
			public void write(final int b) throws IOException
			{
				jtext.append(String.valueOf((char) b));
			}

			@Override
			public void write(byte [] b, int off, int len) throws IOException
			{
				jtext.append(new String(b, off, len));
			}

			@Override
			public void write(byte [] b) throws IOException
			{
				write(b, 0, b.length);
			}
		};
		System.setOut(new PrintStream(out, true));
		System.setErr(new PrintStream(out, true));
	}

	public static Object cloneObject(Object copyObject)
	{
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(copyObject);
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			Object deepCopy = ois.readObject();
			return deepCopy;
		}
		catch ( IOException | ClassNotFoundException e )
		{
			e.printStackTrace();
		}
		return null;
	}

}
