/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.datetime
 *
 * ==============Information==============
 *      Filename: Time.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.datetime;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Time
{

	private String format;

	public Time()
	{
		this.format = "hh:mm:ss";
	}

	public Time(String format)
	{
		this.format = format;
	}

	public String getTime()
	{
		SimpleDateFormat formatter = new SimpleDateFormat(this.format);
		String temp = formatter.format(new Date());
		return temp;
	}

}
