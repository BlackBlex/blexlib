/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.components.customs
 *
 * ==============Information==============
 *      Filename: jLabelImage.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.components.customs;

import java.awt.Cursor;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.JLabel;
import com.blackblex.libs.system.utils.Images;

public class jLabelImage extends JLabel
{

	private static final long serialVersionUID = -3006243432036355170L;
	private Icon icon;
	private String path = "";

	public jLabelImage(String path, Dimension dimension)
	{
		this.path = path;
		this.icon = Images.getImagen(path, this, dimension);
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));
	}

	public void reload(Dimension dimension)
	{
		this.icon = Images.getImagen(this.path, this, dimension);
	}

	public String getPath()
	{
		return this.path;
	}

	@Override
	public Icon getIcon()
	{
		return this.icon;
	}
}
