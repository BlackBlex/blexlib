/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.components.styles
 *
 * ==============Information==============
 *      Filename: JComboBoxStyleMetro.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.components.styles;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.CompoundBorder;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

public class JComboBoxStyleMetro extends BasicComboBoxUI
{

	private ImageIcon espacio = new ImageIcon(getClass().getResource("/com/blackblex/resources/space.png"));
	private Color color;

	private JComboBoxStyleMetro(JComboBox jComboBox)
	{
		init(jComboBox);
		this.color = Color.decode("#3E444C");
	}

	private JComboBoxStyleMetro(JComboBox jComboBox, Color color)
	{
		init(jComboBox);
		this.color = color;
	}

	private void init(JComboBox jComboBox)
	{
		jComboBox.setForeground(Color.WHITE);
		jComboBox.setSize(new Dimension((int) jComboBox.getSize().getWidth(), 40));
		jComboBox.setPreferredSize((new Dimension((int) jComboBox.getSize().getWidth(), 40)));
		jComboBox.setMinimumSize((new Dimension((int) jComboBox.getSize().getWidth(), 40)));
	}

	public static ComboBoxUI createUI(JComboBox jComboBox)
	{
		return new JComboBoxStyleMetro(jComboBox);
	}

	public static ComboBoxUI createUI(JComboBox jComboBox, Color color)
	{
		return new JComboBoxStyleMetro(jComboBox, color);
	}

	@Override
	protected JButton createArrowButton()
	{
		final Color background = this.color, shadow = Color.WHITE, darkShadow = Color.WHITE, highlight = this.color;

		BasicArrowButton basicArrowButton = new BasicArrowButton(BasicArrowButton.SOUTH, background, shadow, darkShadow,
				highlight)
		{
			@Override
			public void paint(Graphics g)
			{
				Color origColor;
				boolean isPressed, isEnabled;
				int w, h, size;

				w = getSize().width;
				h = getSize().height;
				origColor = g.getColor();
				isPressed = getModel().isPressed();
				isEnabled = isEnabled();

				g.setColor(getBackground());
				g.fillRect(1, 1, w - 2, h - 2);

				if ( getBorder() != null && !(getBorder() instanceof UIResource) )
				{
					paintBorder(g);
				}
				else if ( isPressed )
				{
					g.setColor(shadow);
					g.drawRect(0, 0, w - 1, h - 1);
				}
				else
				{
					g.drawLine(0, 0, 0, h - 1);
					g.drawLine(1, 0, w - 2, 0);

					g.setColor(highlight);
					g.drawLine(1, 1, 1, h - 3);
					g.drawLine(2, 1, w - 3, 1);

					g.setColor(shadow);
					g.drawLine(1, h - 2, w - 2, h - 2);
					g.drawLine(w - 2, 1, w - 2, h - 3);

					g.setColor(darkShadow);
					g.drawLine(0, h - 1, w - 1, h - 1);
					g.drawLine(w - 1, h - 1, w - 1, 0);
				}

				if ( h < 5 || w < 5 )
				{
					g.setColor(origColor);
					return;
				}

				if ( isPressed )
				{
					g.translate(1, 1);
				}

				size = Math.min((h - 4) / 3, (w - 4) / 3);
				size = Math.max(size, 2);
				paintTriangle(g, (w - size) / 2, (h - size) / 2, size, direction, isEnabled);

				if ( isPressed )
				{
					g.translate(-1, -1);
				}
				g.setColor(origColor);

			}
		};

		basicArrowButton.setBorder(new CompoundBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0),
				BorderFactory.createLineBorder(this.color, 1)));
		basicArrowButton.setContentAreaFilled(false);
		return basicArrowButton;
	}

	@Override
	public void paintCurrentValueBackground(Graphics g, Rectangle bounds, boolean hasFocus)
	{
		g.setColor(color);
		g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
	}

	@Override
	protected ListCellRenderer createRenderer()
	{
		return new DefaultListCellRenderer()
		{

			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
					boolean cellHasFocus)
			{

				super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				list.setSelectionBackground(color);
				list.setSelectionForeground(Color.WHITE);
				if ( isSelected )
				{
					setBackground(color);
					setForeground(Color.WHITE);
				}
				else
				{
					setBackground(Color.WHITE);
					setForeground(new Color(70, 70, 70));
				}
				if ( index != -1 )
				{
					setIcon(espacio);
				}
				setBorder(BorderFactory.createEmptyBorder(0, 7, 0, 0));
				return this;
			}
		};
	}
}