/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.components.styles
 *
 * ==============Information==============
 *      Filename: JButtonStyleFlat.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.components.styles;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicButtonUI;

public class JButtonStyleFlat extends BasicButtonUI implements MouseListener
{

	private Color backgroundColor, backgroundColorActive, backgroundColorOver, fontColor, borderColor;
	private boolean hover = false;

	public int arcWidth, arcHeight;
	private JButton jbutton;
	
	private String wavClick, wavEnter;

	public JButtonStyleFlat(JButton jButton, String background, int arcw)
	{
		super();

		this.arcWidth = arcw;
		this.arcHeight = arcw;

		this.jbutton = jButton;
		this.jbutton.setFont(new Font(this.jbutton.getFont().getName(), Font.BOLD, 13));
		this.jbutton.setBorderPainted(false);
		setBackgroundColor(background);
		setFontColor(Color.WHITE);
		this.jbutton.setFocusPainted(false);
		this.jbutton.addMouseListener(this);
		
		this.wavClick = "none";
		this.wavEnter = "none";
	}
	
	public JButtonStyleFlat(JButton jButton, String background, int arcw, String wavClick, String wavEnter)
	{
		super();

		this.arcWidth = arcw;
		this.arcHeight = arcw;

		this.jbutton = jButton;
		this.jbutton.setFont(new Font(this.jbutton.getFont().getName(), Font.BOLD, 13));
		this.jbutton.setBorderPainted(false);
		setBackgroundColor(background);
		setFontColor(Color.WHITE);
		this.jbutton.setFocusPainted(false);
		this.jbutton.addMouseListener(this);
		
		this.wavClick = wavClick;
		this.wavEnter = wavEnter;
	}

	@Override
	public void paint(Graphics g, JComponent c)
	{

		if ( hover )
		{
			g.setColor(backgroundColorOver);
			setFontColor(fontColor);
		}
		else if ( !this.jbutton.getModel().isEnabled() )
		{
			g.setColor(Color.decode("#444444"));
			this.jbutton.setForeground(Color.decode("#FFFFFF"));
		}
		else
		{
			g.setColor(backgroundColor);
			setFontColor(fontColor);
		}

		g.fillRoundRect(0, 0, this.jbutton.getSize().width - 1, this.jbutton.getSize().height - 1, this.arcWidth,
				this.arcHeight);

		if ( this.jbutton.getModel().isEnabled() )
		{
			g.setColor(borderColor);
		}
		else
		{
			g.setColor(Color.decode("#444444"));
		}
		g.drawRoundRect(0, 0, this.jbutton.getSize().width - 1, this.jbutton.getSize().height - 1, this.arcWidth,
				this.arcHeight);

		super.paint(g, c);

	}

	@Override
	protected void paintButtonPressed(Graphics g, AbstractButton b)
	{

		g.setColor(backgroundColorActive);
		setFontColor(fontColor);

		g.fillRoundRect(0, 0, this.jbutton.getSize().width - 1, this.jbutton.getSize().height - 1, this.arcWidth,
				this.arcHeight);

		if ( this.jbutton.getModel().isEnabled() )
		{
			g.setColor(borderColor);
		}
		else
		{
			g.setColor(Color.decode("#444444"));
		}
		g.drawRoundRect(0, 0, this.jbutton.getSize().width - 1, this.jbutton.getSize().height - 1, this.arcWidth,
				this.arcHeight);

		// super.paint(g, c);
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		if ( !this.wavClick.startsWith("none") )
		{
			String soundName = this.wavClick;    
			AudioInputStream audioInputStream;
			try
			{
				audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
				Clip clip = AudioSystem.getClip();
				clip.open(audioInputStream);
				clip.start();
			}
			catch ( UnsupportedAudioFileException | IOException | LineUnavailableException e1 )
			{
				e1.printStackTrace();
			}
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		this.jbutton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		this.hover = true;

		if ( !this.wavEnter.startsWith("none") )
		{
			String soundName = this.wavEnter;    
			AudioInputStream audioInputStream;
			try
			{
				audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
				Clip clip = AudioSystem.getClip();
				clip.open(audioInputStream);
				clip.start();
			}
			catch ( UnsupportedAudioFileException | IOException | LineUnavailableException e1 )
			{
				e1.printStackTrace();
			}
		}
		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		this.jbutton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		this.hover = false;
	}

	public void setBackgroundColor(String color)
	{
		backgroundColor = Color.decode(color);
		backgroundColorActive = darker(backgroundColor, 0.8);
		backgroundColorOver = brighter(backgroundColor, 0.05);
		borderColor = backgroundColor;
	}

	public void setBorderColor(String color)
	{
		borderColor = Color.decode(color);
	}

	public void setFontColor(Color color)
	{
		fontColor = color;
		this.jbutton.setForeground(fontColor);
	}

	public static Color brighter(Color color, double fraction)
	{
		int red = (int) Math.round(Math.min(255, color.getRed() + 255 * fraction));
		int green = (int) Math.round(Math.min(255, color.getGreen() + 255 * fraction));
		int blue = (int) Math.round(Math.min(255, color.getBlue() + 255 * fraction));
		int alpha = color.getAlpha();
		return new Color(red, green, blue, alpha);
	}

	public static Color darker(Color color, double fraction)
	{
		int red = (int) Math.round(Math.max(color.getRed() * fraction, 0));
		int green = (int) Math.round(Math.max(color.getGreen() * fraction, 0));
		int blue = (int) Math.round(Math.max(color.getBlue() * fraction, 0));
		int alpha = color.getAlpha();
		return new Color(red, green, blue, alpha);
	}

}
