/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.components.borders
 *
 * ==============Information==============
 *      Filename: RoundedSidesBorder.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.components.borders;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import javax.swing.border.AbstractBorder;

public class RoundedSidesBorder extends AbstractBorder
{

	private static final long serialVersionUID = 9159214321392926211L;
	private Color color;
	private BasicStroke stroke = null;
	private RenderingHints hints;
	private int arcWidth = 15, arcHeight = 0;

	public RoundedSidesBorder()
	{
		this.color = Color.BLACK;
		this.stroke = new BasicStroke(1);
		this.hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	public RoundedSidesBorder(Color color, int stroke, int arcW, int arcH)
	{
		this.color = color;
		this.stroke = new BasicStroke(stroke);
		this.arcHeight = arcH;
		this.arcWidth = arcW;
		this.hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	@Override
	public void paintBorder(Component c, Graphics g, int x, int y, int width, int height)
	{

		Graphics2D g2 = (Graphics2D) g;
		RoundRectangle2D rectangle2D = new RoundRectangle2D.Double(x, y, width, height, this.arcWidth, this.arcHeight);

		if ( this.stroke.getLineWidth() == 0 )
		{
			rectangle2D.setFrameFromCenter(new Point(x + width / 2, y + height / 2), new Point(width, height));
		}
		else
		{
			rectangle2D.setFrameFromCenter(new Point(x + width / 2, y + height / 2),
					new Point(width - (int) this.stroke.getLineWidth(), height - (int) this.stroke.getLineWidth()));
		}

		Polygon pointer = new Polygon();
		Area area = new Area(rectangle2D);
		area.add(new Area(pointer));
		g2.setRenderingHints(this.hints);

		Component parent = c.getParent();
		if ( parent != null )
		{
			Color bg = parent.getBackground();
			Rectangle rect = new Rectangle(0, 0, width, height);
			Area borderRegion = new Area(rect);
			borderRegion.subtract(area);
			g2.setClip(borderRegion);
			g2.setColor(bg);
			g2.fillRect(0, 0, width, height);
			g2.setClip(null);
		}

		if ( this.stroke.getLineWidth() > 0 )
		{
			g2.setColor(this.color);
			g2.setStroke(this.stroke);
		}

		g2.draw(area);
	}
}
