/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package customs
 *
 * ==============Information==============
 *      Filename: FrameWithCustomBorder.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.components.customs;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.blackblex.libs.core.interfaces.jcomponent.FrameInterface;
import com.blackblex.libs.main.Init;

public abstract class FrameWithCustomBorder extends JFrame implements FrameInterface
{

	private static final long serialVersionUID = -7948403284473547246L;
	public JPanel jpanelBarTitle, jpanelHeader, jpanelBody, jpanelBottom;
	public Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	public int FRAME_WIDTH = (int) ((screenSize.width) * 0.50);
	public int FRAME_HEIGHT = (int) ((screenSize.height) * 0.50);
	public final GridBagConstraints gridbag = new GridBagConstraints();

	private Color backgroundBarTitle = Color.decode("#1d1d1d");
	private Color backgroundHover = Color.decode("#545454");
	private Color backgroundPressed = Color.decode("#727272");
	private Color foregroundBarTitle = Color.WHITE;

	private URL pathImageClose = Init.class.getResource("/com/blackblex/resources/buttonClose.png");
	private URL pathImageMaximize = Init.class.getResource("/com/blackblex/resources/buttonMaximize.png");
	private URL pathImageMinimize = Init.class.getResource("/com/blackblex/resources/buttonMinimize.png");

	private int posX, posY, jlabelButtonWidth, jlabelButtonHeight;

	private JLabel jlabelButtonClose, jlabelButtonMinimize, jlabelButtonMaximize, jlabelBarTitle;

	public FrameWithCustomBorder()
	{
		this.setUndecorated(true);

		beforeInit();
		init();
	}

	@Override
	public void beforeInit()
	{
		this.jlabelButtonWidth = ((int) (FRAME_WIDTH * 0.06));
		this.jlabelButtonHeight = ((int) (FRAME_HEIGHT * 0.08));

		jpanelBarTitle = new JPanel();
		jpanelHeader = new JPanel();
		jpanelBody = new JPanel();
		jpanelBottom = new JPanel();

		jlabelBarTitle = new JLabel();
		jlabelButtonClose = new JLabel();
		jlabelButtonMaximize = new JLabel();
		jlabelButtonMinimize = new JLabel();

		jlabelButtonClose.setOpaque(true);
		jlabelButtonMaximize.setOpaque(true);
		jlabelButtonMinimize.setOpaque(true);

		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

		jpanelBarTitle.setLayout(new GridBagLayout());

		jpanelBarTitle.setPreferredSize(new Dimension(screenSize.width+10, (int) (FRAME_HEIGHT * 0.08)));
		jpanelBarTitle.setMaximumSize(new Dimension(screenSize.width+10, (int) (FRAME_HEIGHT * 0.08)));

		jpanelBarTitle.setBorder(new EmptyBorder(0, 10, 0, 0));

		jlabelBarTitle.setPreferredSize(new Dimension((int) (FRAME_WIDTH * 0.91), (int) (FRAME_HEIGHT * 0.08)));
		jlabelBarTitle.setMaximumSize(new Dimension((int) (FRAME_WIDTH * 0.91), (int) (FRAME_HEIGHT * 0.08)));


		jlabelButtonClose.setMaximumSize(new Dimension(this.jlabelButtonWidth, this.jlabelButtonHeight));
		jlabelButtonMaximize.setMaximumSize(new Dimension(this.jlabelButtonWidth, this.jlabelButtonHeight));
		jlabelButtonMinimize.setMaximumSize(new Dimension(this.jlabelButtonWidth, this.jlabelButtonHeight));
		jlabelButtonClose.setSize(new Dimension(this.jlabelButtonWidth, this.jlabelButtonHeight));
		jlabelButtonMaximize.setSize(new Dimension(this.jlabelButtonWidth, this.jlabelButtonHeight));
		jlabelButtonMinimize.setSize(new Dimension(this.jlabelButtonWidth, this.jlabelButtonHeight));

		jpanelHeader.setLayout(new GridBagLayout());
		jpanelHeader.setBorder(new EmptyBorder(10, 10, 10, 10));

		jpanelBody.setLayout(new GridBagLayout());
		jpanelBody.setBorder(new EmptyBorder(10, 10, 10, 10));

		jpanelBottom.setLayout(new GridBagLayout());
		jpanelBottom.setBorder(new EmptyBorder(0, 10, 10, 10));
	}

	@Override
	public void init()
	{
		this.add(jpanelBarTitle);

		gridbag.gridwidth = 3;
		gridbag.weightx = 1;
		gridbag.weighty = 1;
		gridbag.fill = GridBagConstraints.BOTH;
		jpanelBarTitle.add(jlabelBarTitle, gridbag);

		gridbag.gridwidth = 1;
		gridbag.weightx = 0;
		gridbag.weighty = 0;
		jpanelBarTitle.add(jlabelButtonMinimize, gridbag);
		jpanelBarTitle.add(jlabelButtonMaximize, gridbag);
		jpanelBarTitle.add(jlabelButtonClose, gridbag);

		this.add(jpanelHeader);
		this.add(jpanelBody);
		this.add(jpanelBottom);
	}

	@Override
	public void afterInit()
	{
		this.jlabelButtonClose.addMouseListener(new JLabelButtonMouseEventCustom(this.jlabelButtonClose, this.backgroundBarTitle, this.backgroundHover, this.backgroundPressed)
		{

			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				System.exit(0);
			}

		});

		this.jlabelButtonMaximize.addMouseListener(new JLabelButtonMouseEventCustom(this.jlabelButtonMaximize, this.backgroundBarTitle, this.backgroundHover, this.backgroundPressed)
		{

			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				maximizeWindown();
			}

		});

		this.jlabelButtonMinimize.addMouseListener(new JLabelButtonMouseEventCustom(this.jlabelButtonMinimize, this.backgroundBarTitle, this.backgroundHover, this.backgroundPressed)
		{

			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				setExtendedState(JFrame.ICONIFIED);
			}

		});

		this.jpanelBarTitle.addMouseMotionListener(new MouseMotionListener(){

			@Override
			public void mouseDragged(MouseEvent arg0)
			{
				Point mousePos = MouseInfo.getPointerInfo().getLocation();
			    setLocation(mousePos.x - posX, mousePos.y - posY);
			}

			@Override
			public void mouseMoved(MouseEvent arg0)
			{
			}

		});

		this.jpanelBarTitle.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e)
			{
				if ( e.getClickCount() == 2)
					maximizeWindown();
			}

			@Override
			public void mouseEntered(MouseEvent e)
			{
			}

			@Override
			public void mouseExited(MouseEvent e)
			{
			}

			@Override
			public void mousePressed(MouseEvent e)
			{
				posX = e.getX();
				posY = e.getY();
			}

			@Override
			public void mouseReleased(MouseEvent e)
			{
			}

		});

	}

	@Override
	public void finishInit()
	{
		ImageIcon iconClose = new ImageIcon(new ImageIcon(pathImageClose).getImage().getScaledInstance(this.jlabelButtonWidth, this.jlabelButtonHeight, Image.SCALE_SMOOTH));
		ImageIcon iconMaximize = new ImageIcon(new ImageIcon(pathImageMaximize).getImage().getScaledInstance(this.jlabelButtonWidth, this.jlabelButtonHeight, Image.SCALE_SMOOTH));
		ImageIcon iconMinimize = new ImageIcon(new ImageIcon(pathImageMinimize).getImage().getScaledInstance(this.jlabelButtonWidth, this.jlabelButtonHeight, Image.SCALE_SMOOTH));

		jlabelButtonClose.setIcon(iconClose);
		jlabelButtonMaximize.setIcon(iconMaximize);
		jlabelButtonMinimize.setIcon(iconMinimize);

		this.jlabelBarTitle.setForeground(this.foregroundBarTitle);
		this.jlabelBarTitle.setFont(new Font(this.jlabelBarTitle.getFont().getFontName(), Font.BOLD, 14));

		this.jpanelBarTitle.setBackground(this.backgroundBarTitle);

		this.jlabelButtonClose.setBackground(this.backgroundBarTitle);
		this.jlabelButtonMaximize.setBackground(this.backgroundBarTitle);
		this.jlabelButtonMinimize.setBackground(this.backgroundBarTitle);

		this.jpanelHeader.setBackground(Color.WHITE);
		this.jpanelBody.setBackground(Color.WHITE);
		this.jpanelBottom.setBackground(Color.WHITE);
	}

	@Override
	public void showFrame(String nameApp)
	{
		if ( !nameApp.isEmpty() )
			this.jlabelBarTitle.setText(nameApp);

		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	@Override
	public void showFrame(String nameApp, String title)
	{
		if ( !title.isEmpty() )
			this.jlabelBarTitle.setText(nameApp + " ~ " + title);

		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void maximizeWindown()
	{
		if ( this.jlabelButtonMaximize.isVisible() )
		{
	        if( getExtendedState() == JFrame.MAXIMIZED_BOTH )
	        {
	            setExtendedState(JFrame.NORMAL);
	            this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	        }
	        else
	        {
	            Rectangle r = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
	            setMaximizedBounds(r.getBounds());
	            setExtendedState(JFrame.MAXIMIZED_BOTH);
	        }
        }
    }

	public void setForegroundBarTitle(Color color)
	{
		this.foregroundBarTitle = color;
	}

	public void setBackgroundBarTitle(Color color)
	{
		this.backgroundBarTitle = color;
	}

	public void setBackgroundHover(Color color)
	{
		this.backgroundHover = color;
	}

	public void setBackgroundPressed(Color color)
	{
		this.backgroundPressed = color;
	}

	public void setPathImageClose(URL path)
	{
		this.pathImageClose = path;
	}

	public void setPathImageMaximize(URL path)
	{
		this.pathImageMaximize = path;
	}

	public void setPathImageMinimize(URL path)
	{
		this.pathImageMinimize = path;
	}

	@Override
	public void setResizable(boolean resizable)
	{
		super.setResizable(resizable);
		this.jlabelButtonMaximize.setVisible(resizable);
	}

}

abstract class JLabelButtonMouseEventCustom implements MouseListener
{
	private JLabel jbutton;
	private Color background, hover, pressed;

	JLabelButtonMouseEventCustom(JLabel jbutton, Color background, Color hover, Color pressed)
	{
		this.jbutton = jbutton;
		this.background = background;
		this.hover = hover;
		this.pressed = pressed;
	}

	@Override
	public void mouseEntered(MouseEvent evt)
	{
		this.jbutton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		this.jbutton.setBackground(this.hover);
	}

	@Override
	public void mouseExited(MouseEvent evt)
	{
		this.jbutton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		this.jbutton.setBackground(this.background);
	}

	@Override
	public void mousePressed(MouseEvent evt)
	{
		this.jbutton.setBackground(this.pressed);
	}

	@Override
	public void mouseReleased(MouseEvent evt)
	{
		this.jbutton.setBackground(this.background);
	}

}
