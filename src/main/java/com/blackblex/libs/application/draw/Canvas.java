/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.draw
 *
 * ==============Information==============
 *      Filename: Canvas.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.draw;

import com.blackblex.libs.core.interfaces.draw.CanvasInterface;

public class Canvas implements CanvasInterface
{

	private int width;
	private int height;
	private char [] [] canvas;

	public Canvas()
	{
		Init(100, 100);
	}

	public Canvas(int width, int height)
	{
		Init(width, height);
	}

	@Override
	public void Init(int width, int height)
	{
		this.width = width;
		this.height = height;
		this.canvas = new char[height][width];

		for ( int cellY = 0; cellY < height; cellY++ )
			for ( int cellX = 0; cellX < width; cellX++ )
				this.canvas[cellY][cellX] = ' ';
	}

	@Override
	public int getHeight()
	{
		return this.height;
	}

	@Override
	public int getWidth()
	{
		return this.width;
	}

	@Override
	public char [] [] getCanvasArray()
	{
		return this.canvas;
	}

	@Override
	public String paint()
	{
		String paintCanvas = " [H: " + this.height + ", W: " + this.width + "] \n";

		for ( int cellY = 0; cellY < height; cellY++ )
		{
			for ( int cellX = 0; cellX < width; cellX++ )
                paintCanvas += this.canvas[cellY][cellX];
            paintCanvas += "\n";
        }

		return paintCanvas;
	}

	@Override
	public void setPen(int posX, int posY, char character)
	{
		this.canvas[posY][posX] = character;
	}

}
