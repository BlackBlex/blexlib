/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.draw
 *
 * ==============Information==============
 *      Filename: CanvasWithFrame.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.draw;

public class CanvasWithBorder extends Canvas
{

	public CanvasWithBorder()
	{
		super(100, 100);
	}

	public CanvasWithBorder(int width, int height)
	{
		super(width, height);
	}

	@Override
	public void Init(int width, int height)
	{
		super.Init(width, height);

		for ( int cellY = 0; cellY < super.getHeight(); cellY++ )
		{
			for ( int cellX = 0; cellX < super.getWidth(); cellX++ )
			{
				if ( cellY == 0 || cellY == (super.getHeight()-1) )
					super.setPen(cellX, cellY, '-');
				else if ( cellX == 0 || cellX == (super.getWidth()-1) )
					super.setPen(cellX, cellY, '|');
				else
					super.setPen(cellX, cellY, ' ');
			}
		}

	}

}
