/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.timer
 *
 * ==============Information==============
 *      Filename: TimerU.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.timer;

import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

public class TimerU extends Observable
{

	public TimerU()
	{
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(timerTask, 0, 1000);
	}

	public static void majkin(String [] args)
	{
		TimerU modelo = new TimerU();
		modelo.addObserver(new Observer()
		{
			@Override
			public void update(Observable unObservable, Object dato)
			{
				System.out.println(dato);
			}
		});
	}

	TimerTask timerTask = new TimerTask()
	{
		@Override
		public void run()
		{
			setChanged();
			notifyObservers(new Date());
		}
	};
}
