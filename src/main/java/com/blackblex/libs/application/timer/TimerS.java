/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.application.timer
 *
 * ==============Information==============
 *      Filename: TimerS.java
 * ---------------------------------------
*/

package com.blackblex.libs.application.timer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Timer;

public class TimerS extends Observable
{

	public TimerS()
	{
		Timer timer = new Timer(1000, new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				setChanged();
				notifyObservers(new Date());
			}
		});
		timer.start();
	}

	public static void maipn(String [] args)
	{
		TimerS modelo = new TimerS();
		modelo.addObserver(new Observer()
		{
			@Override
			public void update(Observable unObservable, Object dato)
			{
				System.out.println(dato);
			}
		});

		try
		{
			Thread.currentThread();
			Thread.sleep(10000);
		}
		catch ( InterruptedException e )
		{
		}
	}
}
