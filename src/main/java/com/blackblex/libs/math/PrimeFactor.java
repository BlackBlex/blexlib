/**
 * BlexLib
 * 
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.math
 *
 * ==============Information==============
 *      Filename: PrimeFactor.java
 * ---------------------------------------
*/

package com.blackblex.libs.math;

public class PrimeFactor
{

	private int numbers[];
	private String numbersS[];
	private int number, maxIndex, maxIndexS;

	public PrimeFactor()
	{
		number = 100;
		numbers = new int [number];
	}

	public PrimeFactor(int num)
	{
		number = num;
		numbers = new int [number];
	}

	public void generate()
	{
		int index = 0, i, n = number;
		for ( i = 2; i <= n / i; i++ )
		{
			while ( n % i == 0 )
			{
				numbers[index] = i;
				n /= i;
				index++;
			}
		}

		if ( n > 1 )
		{
			numbers[index] = n;
			index++;
		}

		maxIndex = index;
	}

	public void print()
	{
		int i;
		System.out.println("Prime factor of " + number + ":");
		for ( i = 0; i < maxIndex; i++ )
		{
			System.out.println((i + 1) + ") " + numbers[i]);
		}
	}

	public void print(boolean simplify)
	{
		simplify();
		int i;
		System.out.println("Simplify - Prime factor of " + number + ":");
		for ( i = 0; i < maxIndexS; i++ )
		{
			System.out.println((i + 1) + ") " + numbersS[i]);
		}
	}

	public void simplify()
	{
		numbersS = new String [maxIndex];
		int i, counter = 1, index = 0;
		for ( i = 0; i < maxIndex; i++ )
		{
			if ( i + 1 > maxIndex )
			{
				continue;
			}
			if ( numbers[i] == numbers[i + 1] )
			{
				counter++;
			}
			else
			{
				numbersS[index] = numbers[i] + "^" + counter;
				index++;
				counter = 1;
			}
		}
		maxIndexS = index;
	}

	public int getPrimeNumber()
	{
		return maxIndex;
	}

	public void getPrimeNumber(boolean t)
	{
		System.out.println("Numbers prime factors for: " + number + ": " + maxIndex);
	}

	public String getNumber(int index)
	{
		if ( (maxIndexS - 1) < index )
		{
			System.out.print("The index exceeds the maximum index created.");
			return "";
		}
		return numbersS[index];
	}

	public int getLastNumber()
	{
		return numbers[maxIndex - 1];
	}
}
