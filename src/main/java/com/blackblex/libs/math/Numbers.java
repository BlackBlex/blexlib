/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.math
 *
 * ==============Information==============
 *      Filename: Numbers.java
 * ---------------------------------------
*/

package com.blackblex.libs.math;

import java.math.BigInteger;

public class Numbers
{
	public static BigInteger factorial(int n)
	{
		BigInteger num = new BigInteger("1");
		
		for ( int i = 1; i <= n; i++)
			num = num.multiply(new BigInteger(i + ""));
		
		return num;
	}
}
