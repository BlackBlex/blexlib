/**
 * BlexLib
 
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.math.sequences
 *
 * ==============Information==============
 *      Filename: Fibonacci.java
 * ---------------------------------------
*/

package com.blackblex.libs.math.sequences;

public class Fibonacci
{

	private int sequence[];
	private int lastNumber, nextNumber, totalNumber, minNumber, maxNumber, maxIndex;

	public Fibonacci()
	{
		this.minNumber = 1;
		this.maxNumber = 20;
		this.lastNumber = 1;
		this.totalNumber = 0;
		this.sequence = new int [this.maxNumber];
	}

	public Fibonacci(int min, int max)
	{

		if ( min > max )
		{
			this.minNumber = max;
			this.maxNumber = min;
		}
		else
		{
			this.minNumber = min;
			this.maxNumber = max;
		}
		this.lastNumber = 1;
		this.totalNumber = 0;
		this.sequence = new int [(this.maxNumber - this.minNumber) + 1];
	}

	public void generate()
	{

		int index = 0;
		if ( this.minNumber < 1 )
		{
			this.sequence[index] = this.totalNumber;
			index++;

			this.minNumber = this.minNumber + (this.minNumber * -1);
		}
		int i;

		for ( i = 1; i <= this.maxNumber; i++ )
		{
			this.nextNumber = this.totalNumber + this.lastNumber;

			if ( this.minNumber <= i )
			{
				this.sequence[index] = this.nextNumber;
				index++;
			}
			this.lastNumber = this.totalNumber;
			this.totalNumber = this.nextNumber;
		}
		this.maxIndex = index;
	}

	public void print()
	{
		int i;
		for ( i = 0; i < this.maxIndex; i++ )
		{
			System.out.println("Fibo(" + (this.minNumber + i) + ") = " + this.sequence[i]);
		}
	}

	public int getNumber(int index)
	{
		index = index - this.minNumber;
		if ( this.maxIndex < index )
		{
			System.out.print("El index supera el maximo de indice creado.");
			return 0;
		}
		return this.sequence[index];
	}

	public int getLastNumber()
	{
		return this.sequence[this.maxIndex - 1];
	}
}
