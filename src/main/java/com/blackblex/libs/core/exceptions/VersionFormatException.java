/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.exceptions
 *
 * ==============Information==============
 *      Filename: VersionFormatException.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.exceptions;

public class VersionFormatException extends Exception
{
	private static final long serialVersionUID = -5982407756477136300L;
	
	public VersionFormatException(String msg)
	{
		super(msg);
	}
}
