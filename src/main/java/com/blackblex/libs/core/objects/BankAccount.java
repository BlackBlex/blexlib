/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.objects
 *
 * ==============Information==============
 *      Filename: BankAccount.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.objects;

import com.blackblex.libs.main.Init;
import com.blackblex.libs.core.interfaces.objects.BankAccountInterface;
import com.blackblex.libs.core.interfaces.objects.HumanInterface;

public class BankAccount implements HumanInterface, BankAccountInterface
{

	private String DNI = null, NAME = null, BIRTHDAY = null, STREET = null, ACCOUNTTYPE = null, BANK = null,
			DATE = null;
	private int SEX = -1, AGE = -1;
	private long CELLPHONE = -1, ACCOUNTID = -1;
	private double BALANCE = -1;

	public BankAccount()
	{

	}

	@Override
	public String getDNI()
	{
		return this.DNI;
	}

	@Override
	public void setDNI(String dni)
	{
		this.DNI = dni;
	}

	@Override
	public String getName()
	{
		return this.NAME;
	}

	@Override
	public void setName(String name)
	{
		this.NAME = name;
	}

	@Override
	public int getSex()
	{
		return this.SEX;
	}

	@Override
	public void setSex(int sex)
	{
		this.SEX = sex;
	}

	@Override
	public String getBirthday()
	{
		return this.BIRTHDAY;
	}

	@Override
	public void setBirthday(String birthday)
	{
		this.BIRTHDAY = birthday;
	}

	@Override
	public int getAge()
	{
		return this.AGE;
	}

	@Override
	public void setAge(int age)
	{
		this.AGE = age;
	}

	@Override
	public long getCellphone()
	{
		return this.CELLPHONE;
	}

	@Override
	public void setCellphone(long cellphone)
	{
		this.CELLPHONE = cellphone;
	}

	@Override
	public String getStreet()
	{
		return this.STREET;
	}

	@Override
	public void setStreet(String street)
	{
		this.STREET = street;
	}

	@Override
	public long getAccountID()
	{
		return this.ACCOUNTID;
	}

	@Override
	public void setAccountID(long accountid)
	{
		this.ACCOUNTID = accountid;
	}

	@Override
	public String getAccountType()
	{
		return this.ACCOUNTTYPE;
	}

	@Override
	public void setAccountType(String accounttype)
	{
		this.ACCOUNTTYPE = accounttype;
	}

	@Override
	public String getBank()
	{
		return this.BANK;
	}

	@Override
	public void setBank(String bank)
	{
		this.BANK = bank;
	}

	@Override
	public double getBalance()
	{
		return this.BALANCE;
	}

	@Override
	public void setBalance(double balance)
	{
		this.BALANCE = balance;
	}

	@Override
	public String getDate()
	{
		return this.DATE;
	}

	@Override
	public void setDate(String date)
	{
		this.DATE = date;
	}

	@Override
	public void deposit(double money)
	{
		setBalance(getBalance() + money);
		setDate(Init.getDate());
	}

	@Override
	public void withdrawal(double money)
	{
		setBalance(getBalance() - money);
		setDate(Init.getDate());
	}

	@Override
	public String toString()
	{
		String split = " ~ ";

		return this.getAccountID() + split + this.getAccountType() + split + this.getBank() + split + this.getBalance()
				+ split + this.getDate() + split + this.getName() + split + this.getDNI() + split + this.getSex()
				+ split + this.getBirthday() + split + this.getAge() + split + this.getCellphone() + split
				+ this.getStreet();
	}
}
