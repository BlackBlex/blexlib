/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.objects.customs
 *
 * ==============Information==============
 *      Filename: BankAccountFile.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.objects.customs;

import com.blackblex.libs.core.file.Files;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.core.objects.BankAccount;

public class BankAccountFile extends BankAccount
{

	private Files accountFile = new Files();

	public void getAccountInfo()
	{
		try
		{
			this.setAccountID(Long.parseLong(this.accountFile.readLine(1)));
			this.setAccountType(this.accountFile.readLine(2));
			this.setBank(this.accountFile.readLine(3));
			this.setBalance(Double.parseDouble(this.accountFile.readLine(4)));
			this.setDate(this.accountFile.readLine(5));

			this.setName(this.accountFile.readLine(6));
			this.setDNI(this.accountFile.readLine(7));
			this.setSex(Integer.parseInt(this.accountFile.readLine(8)));
			this.setBirthday(this.accountFile.readLine(9));
			this.setAge(Integer.parseInt(this.accountFile.readLine(10)));
			this.setCellphone(Long.parseLong(this.accountFile.readLine(11)));
			this.setStreet(this.accountFile.readLine(12));
		}
		catch ( NumberFormatException e )
		{
			Init.MSG.printMsg("Error al recuperar los datos");
		}
	}

	public void setAccountInfo(String file)
	{
		this.accountFile.init(file);
	}

	public boolean delete()
	{
		return this.accountFile.delete();
	}

	public boolean save()
	{
		if ( this.accountFile.exist() )
		{
			delete();
		}

		this.accountFile.append(Long.toString(this.getAccountID()), true);
		this.accountFile.append(this.getAccountType(), true);
		this.accountFile.append(this.getBank(), true);
		this.accountFile.append(Double.toString(this.getBalance()), true);
		this.accountFile.append(this.getDate(), true);

		this.accountFile.append(this.getName(), true);
		this.accountFile.append(this.getDNI(), true);
		this.accountFile.append(Integer.toString(this.getSex()), true);
		this.accountFile.append(this.getBirthday(), true);
		this.accountFile.append(Integer.toString(this.getAge()), true);
		this.accountFile.append(Long.toString(this.getCellphone()), true);
		this.accountFile.append(this.getStreet(), true);

		return this.accountFile.exist();
	}
}
