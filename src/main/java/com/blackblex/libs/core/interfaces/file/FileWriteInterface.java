/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.interfaces.file
 *
 * ==============Information==============
 *      Filename: FileWriteInterface.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.interfaces.file;

public interface FileWriteInterface
{

	void write(String txt, boolean newLine);

	void append(String txt, boolean newLine);

	void write(String txt);

	void append(String txt);

}
