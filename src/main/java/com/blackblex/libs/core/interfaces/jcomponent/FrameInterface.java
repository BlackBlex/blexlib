/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.interfaces.jcomponent
 *
 * ==============Information==============
 *      Filename: FrameInterface.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.interfaces.jcomponent;

public interface FrameInterface
{

	public void beforeInit();

	public void init();

	public void afterInit();

	public void finishInit();

	public void showFrame(String title);
	
	public void showFrame(String nameApp, String title);

}
