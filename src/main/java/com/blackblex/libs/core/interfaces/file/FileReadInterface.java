/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.interfaces.file
 *
 * ==============Information==============
 *      Filename: FileReadInterface.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.interfaces.file;

public interface FileReadInterface
{

	String read();

	String readLine(int line);

}