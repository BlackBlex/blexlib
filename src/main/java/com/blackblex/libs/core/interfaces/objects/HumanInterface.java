/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.libs.core.interfaces.objects
 *
 * ==============Information==============
 *      Filename: HumanInterface.java
 * ---------------------------------------
*/

package com.blackblex.libs.core.interfaces.objects;

public interface HumanInterface
{

	String getDNI();

	void setDNI(String dni);

	String getName();

	void setName(String name);

	int getSex();

	void setSex(int sex);

	String getBirthday();

	void setBirthday(String birthday);

	int getAge();

	void setAge(int age);

	long getCellphone();

	void setCellphone(long cellphone);

	String getStreet();

	void setStreet(String street);

}
