/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.core
 *
 * ==============Information==============
 *      Filename: GlobalService.java
 * ---------------------------------------
*/

package com.blackblex.plugins.core;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;

public class GlobalService
{

	private Container container = null;
	private Map <String, Object> objects = new LinkedHashMap <>();

	private GridBagConstraints gbc = new GridBagConstraints();

	public Container getContainer()
	{
		return this.container;
	}

	public void setContainer(Container jc)
	{
		this.container = jc;
	}

	public void addToContainer(String JPanelPluginContainer, JComponent comp)
	{
		if ( getContainer() != null )
		{
			List <Component> components = getAllComponents(getContainer());
			for ( Component com : components )
			{
				try
				{
					if ( com.getName().contains(JPanelPluginContainer)
							|| com.getClass().getCanonicalName().contains(JPanelPluginContainer) )
					{
						this.gbc.insets = new java.awt.Insets(0, 0, 0, 4);
						this.gbc.fill = GridBagConstraints.BOTH;
						JPanelPluginContainer jpcont = (JPanelPluginContainer) com;
						jpcont.addToContainer(comp, this.gbc);
					}
				}
				catch ( NullPointerException ex )
				{
				}
			}
		}
	}

	public Object getObject(String objectName)
	{
		return this.objects.get(objectName);
	}

	public void addObject(String objectName, Object object)
	{
		this.objects.put(objectName, object);
	}

	private List <Component> getAllComponents(final Container c)
	{
		Component [] comps = c.getComponents();
		List <Component> compList = new ArrayList <>();
		for ( Component comp : comps )
		{
			compList.add(comp);
			if ( comp instanceof Container )
			{
				compList.addAll(getAllComponents((Container) comp));
			}
		}
		return compList;
	}

}
