/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.core
 *
 * ==============Information==============
 *      Filename: JPanelPluginContainer.java
 * ---------------------------------------
*/

package com.blackblex.plugins.core;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import javax.swing.JPanel;

public class JPanelPluginContainer extends JPanel
{

	private static final long serialVersionUID = -621541827517767812L;
	private final ArrayList <PluginCore> pluginList = new ArrayList <>();

	public JPanelPluginContainer()
	{
		setLayout(new GridBagLayout());
	}

	public ArrayList <PluginCore> getPluginList()
	{
		return this.pluginList;
	}

	public void addToContainer(PluginCore plugin, Component comp, GridBagConstraints gbc)
	{
		this.pluginList.add(plugin);
		add(comp, gbc);
		revalidate();
		updateUI();
	}

	public void addToContainer(Component comp, GridBagConstraints gbc)
	{
		add(comp, gbc);
		revalidate();
		updateUI();
	}

	public void removeToContainer(PluginCore plugin, Component comp)
	{
		this.pluginList.remove(plugin);
		remove(comp);
		revalidate();
		updateUI();
	}
}
