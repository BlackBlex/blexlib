/**
 * BlexLib
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package com.blackblex.plugins.core.interfaces
 *
 * ==============Information==============
 *      Filename: IPlugin.java
 * ---------------------------------------
*/

package com.blackblex.plugins.core.interfaces;

public interface IPlugin
{
	boolean load();

	default boolean unload()
	{
		return true;
	}

	boolean start();

	default boolean before()
	{
		return true;
	}

	default boolean init()
	{
		return true;
	}

	default boolean after()
	{
		return true;
	}

	boolean end();
}
