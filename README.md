**BlexLib**
===================


Es una libreria en java que contiene metodos que facilitan el desarrollo.

Igual incluye interfaces para realizar implementaciones en caso de necesitarlo.


**Nota:** *Se irá añadiento más metodos conforme se vaya avanzando en su desarrollo, por el momento solo cuenta con algunos metodos.*

--------
 BlexLib | Libreria básica

 Libreria escrita en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/