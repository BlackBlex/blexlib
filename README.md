**BlexLib**
===================


Es una libreria en java que contiene metodos que facilitan el desarrollo.

Igual incluye interfaces para realizar implementaciones en caso de necesitarlo.


**Nota:** *Se irá añadiento más metodos conforme se vaya avanzando en su desarrollo, por el momento solo cuenta con algunos metodos.*

**Nota 2:** *El uso del sistema de plugins esta recomendado solamente para Java 8, no funciona en versiones posteriores*

*Importante:* La rama (main) contiene codigo solo para ejecutar en Java 9 en adelante, si quiere usar Java 8, use la rama Java 8

--------
 BlexLib | Libreria básica

 Libreria escrita en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/
